<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Tammi project</title>
<?php echo $this->tag->stylesheetLink('css/bootstrap.min.css'); ?>
    <?php echo $this->tag->stylesheetLink('css/bootstrap-theme.min.css'); ?>
    <?php echo $this->tag->stylesheetLink('css/style.css'); ?>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<?php echo $this->tag->javascriptInclude('js/html5shiv.js'); ?>
<?php echo $this->tag->javascriptInclude('js/respond.min.js'); ?>
<![endif]-->
</head>
<body>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?php echo $this->tag->linkTo(array('', 'Tammi', 'class' => 'navbar-brand')); ?>
        </div>
        <?php if (!$user['authorized']) { ?>
            <?php echo $this->tag->form(array('session/start', 'method' => 'post', 'class' => 'navbar-form navbar-left')); ?>
            <div class="form-group">
                <input type="email" class="form-control" name="email" placeholder="Email">
            </div>
            <div class="form-group">
                <?php echo $this->tag->passwordField(array('password', 'class' => 'form-control', 'placeholder' => 'Password')); ?>
            </div>
            <?php echo $this->tag->hiddenField(array('token', 'name' => $this->security->getTokenKey(), 'value' => $this->security->getToken())); ?>
            <?php echo $this->tag->submitButton(array('Login', 'class' => 'btn btn-default')); ?>
            <?php echo $this->tag->endForm(); ?>
        <?php } ?>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <?php foreach (array('city', 'company', 'tariff') as $entity) { ?>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $entity; ?> <b
                                    class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><?php echo $this->tag->linkTo(array($entity . '/search', 'List')); ?></li>
                            <li><?php echo $this->tag->linkTo(array($entity . '/new', 'New')); ?></li>
                            <li class="divider"></li>
                            <li><?php echo $this->tag->linkTo(array($entity . '/', 'Search')); ?></li>
                        </ul>
                    </li>
                <?php } ?>
                <li><a href="#">Stats</a></li>
                <?php if ($user['authorized']) { ?>
                    <li><?php echo $this->tag->linkTo(array('session/end', $user['email'])); ?></li>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>
<div class="container-fluid">
    <?php echo $this->getContent(); ?>
</div>
<?php echo $this->tag->javascriptInclude('js/jquery-2.1.3.min.js'); ?>
<?php echo $this->tag->javascriptInclude('js/bootstrap.min.js'); ?>
</body>
</html>