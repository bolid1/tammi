<!-- PRELOAD -->
<div id="preload" class="gradient">
    <div class="svg">
        <img class="logo" src="{{ url('img/landing/logo_w.png') }}" alt="logo">
        <img class="preload" src="{{ url('img/landing/preload.svg') }}" alt="loading">
    </div>
</div>

<!-- NAVIGATION -->
<nav id="nav-primary" class="navbar navbar-custom navbar-fixed-top" role="navigation">
    <div class="container">
        <div data-scroll-header class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#nav">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            {{ link_to('', '<img src="' ~ url('img/landing/logo.png') ~ '" alt="logo">') }}
        </div>

        <div class="collapse navbar-collapse" id="nav">
            <ul class="nav navbar-nav navbar-right">

                <li>{{ link_to('docs/tammi.pdf', 'Презентация', 'target': '_blank') }}</li>
                <li><a data-scroll href="#footer">Связаться</a></li>
            </ul>
        </div>
    </div>
</nav>

<!-- TOP -->
<section id="top">
    <div class="container">
        <div class="row">

            <div class="col-md-5 col-lg-5 hidden-sm hidden-xs">
                <h2 class="hidden">Tammi</h2>

                <div id="trigger1">
                    <img id="pin1" class="img-responsive" src="{{ url('img/landing/iphone6.png') }}" alt="">
                </div>
            </div>

            <div class="col-md-7 col-lg-7">
                <img class="logo" src="{{ url('img/landing/logo_w.png') }}" alt="logo">

                <p class="lead">Tammi - это автоматизированный агрегатор такси.<br>Мы узнаем маршрут поездки,
                    анализируем её стоимость в различных таксопарках и сообщаем результат.</p>
                <a class="btn button-big button-dark" target="_blank"
                   href="https://itunes.apple.com/ru/app/tammi-vygodnyj-zakaz-taksi/id965748727?mt=8"><i
                            class="fa fa-apple"></i> Скачать!</a>
                <a data-scroll class="btn button-big button-light" href="#features-1">О проекте</a>
            </div>

        </div>
    </div>
</section>

<!-- FEATURES 1 -->
<section id="features-1">
    <div class="container">
        <div class="row">

            <div class="col-md-10 col-md-offset-1">
                <div class="row">

                    <div class="col-md-7 col-md-offset-5">

                        <!-- feature -->
                        <div class="feature wow fadeInDown">
                            <div class="row">
                                <div class="col-lg-2">
                                    <i class="pe pe-5x pe-7s-car"></i>
                                </div>

                                <div class="col-lg-10">
                                    <h3>Прозрачность</h3>

                                    <p>Наш клиент изначально знает, что воспользуется услугами конкретного
                                        таксопарка.<br>
                                        Теперь не будет такого, что проблемы других таксопарков влияют на вашу
                                        репутацию.</p>
                                </div>
                            </div>
                        </div>

                        <!-- feature -->
                        <div class="feature wow fadeInDown" data-wow-delay="0.1s">
                            <div class="row">
                                <div class="col-lg-2">
                                    <i class="pe pe-5x pe-7s-users"></i>
                                </div>

                                <div class="col-lg-10">
                                    <h3>Честность</h3>

                                    <p>Мы не будем демпинговать и давить на таксопарк своими тарифами.<br>Нам это вообще
                                        не интересно.</p>
                                </div>
                            </div>
                        </div>

                        <!-- feature -->
                        <div class="feature wow fadeInDown" data-wow-delay="0.2s">
                            <div class="row">
                                <div class="col-lg-2">
                                    <i class="pe pe-5x pe-7s-rocket"></i>
                                </div>

                                <div class="col-lg-10">
                                    <h3>Сотрудничество</h3>

                                    <p>Чем больше заказов мы вам приводим - тем выгоднее наше сотрудничество.</p>
                                </div>
                            </div>
                        </div>

                        <!-- feature -->
                        <div class="feature wow fadeInDown" data-wow-delay="0.3s">
                            <div class="row">
                                <div class="col-lg-2">
                                    <i class="pe pe-5x pe-7s-plug"></i>
                                </div>

                                <div class="col-lg-10">
                                    <h3>Простота</h3>

                                    <p>Подключение займёт всего пару часов.</p>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

        </div>
    </div>
</section>

<!-- INFO SECTION -->
<section id="info">
    <div class="container">
        <div class="row">

            <div class="col-md-4 col-lg-4 text-center wow fadeInLeft">
                <div class="icon gradient">
                    <i class="pe pe-5x pe-7s-wristwatch"></i>
                </div>
                <h2 class="grd">Быстро</h2>

                <p>Достаточно связаться с нами и через пару дней вы начнете получать заказы.</p>
            </div>

            <div class="col-md-4 col-lg-4 text-center wow fadeInUp">
                <div class="icon gradient">
                    <i class="pe pe-5x pe-7s-shield"></i>
                </div>
                <h2>Безопасно</h2>

                <p>Наши системы не загружают данных о заказах, которые приведены не нами.</p>
            </div>

            <div class="col-md-4 col-lg-4 text-center wow fadeInRight">
                <div class="icon gradient">
                    <i class="pe pe-5x pe-7s-plugin"></i>
                </div>
                <h2>Интегрированно</h2>

                <p>Вы легко сможете видеть, какие заказы приходят к вам из Tammi.</p>
            </div>

        </div>
    </div>
</section>

<!-- FEATURES 2 -->
<section id="features-2">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="row">

                    <div class="col-md-7 col-lg-7">

                        <div class="feature wow fadeInDown">
                            <h2 class="text-center">Почему с нами выгодно работать?</h2>

                            <p class="lead text-center">Потому что мы приводим вам новых клиентов, но при этом не влияем
                                на ваши внутренние правила и тарифы.</p>
                        </div>

                        <!-- Feature -->
                        <div class="feature wow fadeInLeft" data-wow-delay="0.2s">
                            <div class="row">
                                <div class="col-lg-2">
                                    <i class="pe pe-5x pe-7s-smile"></i>
                                </div>

                                <div class="col-lg-10">
                                    <h3>Нет скрытых требований</h3>

                                    <p>Мы оставляем заботу клиента на ваших плечах и не требуем исполнения наших
                                        правил.</p>
                                </div>
                            </div>
                        </div>

                        <!-- Doughnut Chart -->
                        <div class="feature wow fadeInRight">
                            <div class="row">
                                <div class="col-lg-2">
                                    <i class="pe pe-5x pe-7s-bell"></i>
                                </div>

                                <div class="col-lg-10">
                                    <h3>Нет штрафов</h3>

                                    <p>Мы не штрафуем вас за какие-либо нарушения, оставим это полиции.</p>
                                </div>
                            </div>
                        </div>

                        <div class="feature wow fadeInRight">
                            <div class="row">
                                <div class="col-lg-2">
                                    <i class="pe pe-5x pe-7s-cash"></i>
                                </div>

                                <div class="col-lg-10">
                                    <h3>Попробуйте бесплатно</h3>

                                    <p>До 15 августа 2015 мы просто бесплатно отдаём вам заказы. Достаточно лишь
                                        подключиться и можно начинать зарабатывать больше.</p>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="col-sm-12 col-md-5 col-lg-5">
                        <div id="trigger2">
                            <img id="pin2" class="img-responsive wow fadeInRight" src="{{ url('img/landing/iphone6_34.png') }}"
                                 alt="">
                        </div>
                    </div>


                </div>
            </div>

        </div>
    </div>
</section>

<!-- FOOTER -->
<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <img class="logo" src="{{ url('img/landing/logo_w.png') }}" alt="">

                <p class="">Мы с радостью ответим на все ваши вопросы</p>

                <div class="row">
                    <div class="input-group input-group-lg col-lg-6 col-lg-offset-3">
                        <input type="email" id="newsletter_email" class="form-control input-lg input-submit"
                               placeholder="Введите ваш email" data-send_to="{{ url('add/feedback') }}">
                                <span class="input-group-btn">
                                    <button class="btn button-light" onclick="newsletter_send();">Связаться!</button>
                                </span>
                    </div>
                </div>

                <p class="lead">hello@tammiapp.ru</p>

                <p class="small">Tammi LLC 2015</p>
            </div>
        </div>
    </div>
</footer>



