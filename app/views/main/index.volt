<!DOCTYPE html>
<html>
<head>
    <title>Tammi - новое слово в мире такси</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
    <meta name="description" content="Pixomic - Landing Page">
    <meta name="author" content="themecube">

    <!-- viewport settings -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>


    {{ stylesheet_link("css/bootstrap.min.css") }}
    {{ stylesheet_link("css/bootstrap-theme.min.css") }}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    {{ javascript_include("js/html5shiv.js") }}
    {{ javascript_include("js/respond.min.js") }}
    <![endif]-->
    {{ javascript_include("js/landing/modernizr.custom.js") }}

    <!-- CSS -->
    {{ stylesheet_link("css/bootstrap.min.css") }}

    {{ stylesheet_link("css/landing/pe-icon-7-stroke.css") }}
    {{ stylesheet_link("css/landing/helper.css") }}
    {{ stylesheet_link("css/landing/animate.min.css") }}
    {{ stylesheet_link("css/landing/font-awesome.css") }}
    {{ stylesheet_link("css/landing/font-awesome.css") }}
    {{ stylesheet_link("css/landing/lato.css") }}
    {{ stylesheet_link("css/landing/owl.carousel.css") }}
    {{ stylesheet_link("css/landing/owl.theme.css") }}
    {{ stylesheet_link("css/landing/owl.transitions.css") }}
    {{ stylesheet_link("css/landing/main.css") }}
    {{ stylesheet_link("css/landing/color/gradient-orange.css") }}
    {{ stylesheet_link("css/landing/image-bg.css") }}

    <link href='http://fonts.googleapis.com/css?family=Roboto&subset=latin,cyrillic' rel='stylesheet' type='text/css'>

    <!-- choose your gradient colors -->
    <!-- orange, magenta, blue, violet, turqouise -->

    <!-- image background -->

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ url('img/landing/favicon.png') }}">
</head>
<body>
{{ content() }}


{{ javascript_include("js/jquery-2.1.3.min.js") }}
{{ javascript_include("js/bootstrap.min.js") }}
{{ javascript_include("js/landing/bind-polyfill.min.js") }}
{{ javascript_include("js/landing/smooth-scroll.min.js") }}
{{ javascript_include("js/landing/Chart.min.js") }}
{{ javascript_include("js/landing/owl.carousel.min.js") }}
{{ javascript_include("js/landing/wow.min.js") }}
{{ javascript_include("js/landing/retina.min.js") }}
{{ javascript_include("js/landing/iscroll-probe.js") }}
{{ javascript_include("js/landing/jquery.ScrollMagic.min.js") }}
{{ javascript_include("js/landing/main.js") }}
{{ javascript_include("js/landing/ga.js") }}
</body>
</html>