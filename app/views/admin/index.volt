<!DOCTYPE html>
<html lang="ru">
<head>
    {% include 'common/template/head.volt' %}
</head>
<body>
{% include 'common/template/header.volt' %}
<div class="container-fluid">
    {{ content() }}
</div>
{{ javascript_include("js/jquery-2.1.3.min.js") }}
{{ javascript_include("js/bootstrap.min.js") }}
{% for java_script in  template['java_scripts'] %}
    {{ javascript_include("js/" ~ java_script) }}
{% endfor %}
</body>
</html>