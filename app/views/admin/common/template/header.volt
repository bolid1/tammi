<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            {{ link_to('', 'Tammi', 'class': 'navbar-brand') }}
        </div>

        {% if not user['authorized'] %}
            {{ form('session/start', 'method': 'post', 'class': 'navbar-form navbar-left') }}
            <div class="form-group">
                <input type="email" class="form-control" name="email" placeholder="Email">
            </div>
            <div class="form-group">
                {{ password_field('password', 'class': 'form-control', 'placeholder': 'Password') }}
            </div>
            {{ hidden_field('token_key', 'name': 'token_key', 'value': template['security']['token']['key']) }}
            {{ hidden_field('token_value', 'name': 'token_value', 'value': template['security']['token']['value']) }}
            {{ submit_button('Login', 'class': 'btn btn-default') }}
            {{ end_form() }}
        {% endif %}
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                {% for item in template['menu']['items'] %}
                    {% include 'common/template/menu/item' with ['item':item] %}
                {% endfor %}
            </ul>
        </div>
    </div>
</div>