{% if item['type'] is defined and item['type'] === 'drop_down' %}
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            {{ item['text'] }}
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            {% for item in item['items'] %}
                {% if item['divider'] %}
                    <li class="divider"></li>
                {% endif %}
                {% include 'common/template/menu/item' with ['item': item] %}
            {% endfor %}
        </ul>
    </li>
{% else %}
    <li>{{ link_to(item['href'], item['text']) }}</li>
{% endif %}