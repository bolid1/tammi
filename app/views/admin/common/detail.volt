{{ form(entity ~ '/' ~ action, 'class': 'form-horizontal', 'id': entity ~ '_form') }}
    {% block form_inner %}
        {% for field in fields %}
            {% include 'common/controls/form/field' with ['field': field] %}
        {% endfor %}
    {% endblock %}

    {% block form_inner_after %}
    {% endblock %}

    {% block form_button %}
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                {{ submit_button(lang._('Save'), 'class': 'btn btn-default') }}
            </div>
        </div>
    {% endblock %}
{{ end_form() }}

{% block form_after %}
{% endblock %}