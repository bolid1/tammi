<div class="table-responsive">
    <table class="table table-condensed table-hover">
        <caption>{{ lang._(entity) }} list</caption>
        <thead>
        <tr>
            {% for field, field_name in fields %}
                <th>{{ field_name }}</th>
            {% endfor %}
        </tr>
        </thead>
        <tbody>
        {% for item in data.items %}
            <tr>
                {% for field, field_name in fields %}
                    {% if field === 'id' %}
                        <th scope="row">{{ item.current(field) }}</th>
                    {% else %}
                        <td>{{ item.current(field) }}</td>
                    {% endif %}
                {% endfor %}
                <td>{{ link_to(entity ~ '/edit/' ~ item.current('id'), lang._('Edit')) }}</td>
                <td>{{ link_to(entity ~ '/delete/' ~ item.current('id'), lang._('Delete')) }}</td>
            </tr>
        {% endfor %}
        </tbody>
    </table>
</div>
{% if data.total_pages %}
    {% include 'common/list/pagination' with ['current': data.current, 'total': data.total_pages] %}
{% endif %}