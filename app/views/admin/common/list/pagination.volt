{% set prev = current-1 %}
{% if prev < 1 %}
    {% set prev = 0 %}
{% endif %}

{% set next = current+1 %}
{% if next > total %}
    {% set next = 0 %}
{% endif %}

<nav>
    <ul class="pagination">
        <li class="{% if not prev %}disabled{% endif %}">
            <a href="{{ url(entity ~ '/index/?page=' ~ prev) }}" aria-label="{{ lang._('Previous') }}">
                <span aria-hidden="true">&laquo;</span>
            </a>
        </li>

        {% for counter in 1..total %}
            <li class="{% if counter == current %}active{% endif %}">
                {{ link_to(entity ~ '/index/?page=' ~ counter, counter) }}
            </li>
        {% endfor %}

        <li class="{% if not next %}disabled{% endif %}">
            <a href="{{ url(entity ~ '/index/?page=' ~ next) }}" aria-label="{{ lang._('Next') }}">
                <span aria-hidden="true">&raquo;</span>
            </a>
        </li>
    </ul>
</nav>