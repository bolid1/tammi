{% if not (label is defined) %}
    {% set label = field %}
{% endif %}
{% if form_field_attributes(field)['control-type'] === 'hiddenField' %}
    {{ form_field(field) }}
{% else %}
    <div class="form-group">
        <label for="{{ field }}" class="col-sm-2 col-lg-offset-2 col-lg-2 control-label">{{ lang._(label) }}</label>

        <div class="col-sm-10 col-lg-6">
            {{ form_field(field) }}
        </div>
    </div>
{% endif %}