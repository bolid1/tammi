{% extends 'common/detail.volt' %}
{% block form_inner_after %}
    {% for tariff_time in tariff_times %}
        <div class="form-group col-sm-3 js-tariff_time {% if tariff_time == 'tariff_time[new]' %}js-tariff_time_new{% endif %}">
            {% for field_name in tariff_time_fields %}
                {% set field = tariff_time ~ '[' ~ field_name ~ ']' %}
                {% if form_field_attributes(field) %}
                    {% include 'common/controls/form/field' with ['field': field, 'label': field_name] %}
                {% endif %}
            {% endfor %}
        </div>
    {% endfor %}

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            {% for day, name in days %}
                <label for="day_{{ day }}">{{ name }}</label>
                <input id="day_{{ day }}" class="js-day" type="checkbox" value="{{ day }}" checked="checked">
            {% endfor %}
            <button type="button" id="add_time_btn">{{ lang._('Add time') }}</button>
        </div>
    </div>
{% endblock %}