
{{ form('user/push', 'method': 'post', 'class': '') }}
<div class="form-group">
    <label for="prod">Prod:</label>
    {{ radio_field('prod', 'name': 'type', 'value' : 'prod', 'class': 'form-control') }}
</div>
<div class="form-group">
    <label for="dev">Dev:</label>
    {{ radio_field('dev', 'name': 'type', 'value' : 'dev', 'class': 'form-control', 'checked': 'true') }}
</div>
<div class="form-group">
    <label for="message">Message:</label>
    {{ text_field('message', 'name': 'message', 'value' : 'Test', 'class': 'form-control') }}
</div>
<div class="form-group">
    <label for="message">User id:</label>
    {{ text_field('message', 'name': 'user_id', 'value' : '5', 'class': 'form-control') }}
</div>
{{ submit_button('send', 'class': 'btn btn-default') }}
{{ end_form() }}