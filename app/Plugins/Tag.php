<?php
namespace Plugins;


class Tag extends \Phalcon\Tag {
    protected $_default_attributes = [];

    protected function prepare_parameters($parameters) {
        // Приведение к массиву
        if (!is_array($parameters)) {
            $parameters = [$parameters];
        } elseif (!isset($parameters[0])) {
            if (isset($parameters['name'])) {
                $parameters[0] = $parameters['name'];
            } elseif (isset($parameters['id'])) {
                $parameters[0] = $parameters['id'];
            }
        }

        // Допишем то, чего не хватает
        $parameters += [
            'id' => $parameters[0],
            'name' => $parameters[0],
        ];

        return $parameters;
    }

    public function set_default($name, $attributes) {
        $this->_default_attributes[$name] = $attributes;
    }

    public function set_defaults($parameters) {
        $this->_default_attributes = $parameters + $this->_default_attributes;
    }

    public function clear_defaults($names = []) {
        if (empty($names)) {
            $this->_default_attributes = [];
        } else {
            if (!is_array($names)) {
                $names = [$names];
            }

            foreach ($names as $name) {
                unset($this->_default_attributes[$name]);
            }
        }
    }

    public function form_field($parameters) {
        $parameters = $this->prepare_parameters($parameters);
        if (isset($this->_default_attributes[$parameters['name']])) {
            // Допишем аттрибуты
            $parameters += $this->_default_attributes[$parameters['name']];
        }

        if (!isset($parameters['control-type']) || !method_exists(__CLASS__, $parameters['control-type'])) {
            $parameters['control-type'] = 'textField';
        }

        $method = $parameters['control-type'];
        unset($parameters['control-type']);

        $data = (isset($parameters['control-data']) ? $parameters['control-data'] : NULL);
        unset($parameters['control-data']);

        if (is_null($data)) {
            $result = $this->$method($parameters);
        } else {
            $result = $this->$method($parameters, $data);
        }

        return $result;
    }

    public function form_field_attributes($field_name) {
        if (isset($this->_default_attributes[$field_name])) {
            // Допишем аттрибуты
            $result = $this->_default_attributes[$field_name];
        } else {
            $result = [];
        }

        return $result;
    }
}