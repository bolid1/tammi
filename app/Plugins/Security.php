<?php
namespace Plugins;

use Phalcon\Events\Event,
    Phalcon\Mvc\Dispatcher,
    Phalcon\Mvc\User\Plugin,
    Phalcon\Acl;

class Security extends Plugin {
    public function beforeExecuteRoute(Event $event, Dispatcher $dispatcher) {
        //Take the active controller/action from the dispatcher
        $controller = $dispatcher->getControllerName();
        $action = $dispatcher->getActionName();

        $allowed = $this->has_access($controller, $action);
        if (!$allowed) {

            //If he doesn't have access forward him to the index controller
            $this->flash->error("You don't have access to this module");
            $this->response->redirect('index/index', FALSE, 403);

            //Returning "false" we tell to the dispatcher to stop the current operation
            return FALSE;
        }

        return TRUE;
    }

    public function has_access($controller, $action) {
        //Check whether the "auth" variable exists in session to define the active role
        $auth = $this->session->get('auth');
        if (!$auth) {
            $role = 'Guest';
        } elseif ($auth && $auth['role'] === 'Admin') {
            $role = 'Admin';
        } else {
            $role = 'User';
        }

        /** @var Acl\AdapterInterface $acl */
        $acl = $this->di->get('Acl');
        //Check if the Role have access to the controller (resource)
        $allowed = $acl->isAllowed($role, $controller, $action);

        return $allowed;
    }
}