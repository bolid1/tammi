<?php

use Phalcon\Mvc\View;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
use Phalcon\Mvc\Dispatcher as PhDispatcher;
use Phalcon\DI\FactoryDefault;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\Session\Adapter\Files as SessionAdapter;
use Phalcon\Filter as PhFilter;

use Libs\Maps as LMaps;
use Libs\Integration as LIntegration;

use Plugins\Security;

/**
 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
 */
$di = new FactoryDefault();
$di->set('tag', 'Plugins\Tag');

$di->set('config', $config);

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->set('url', function () use ($config) {
    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);

    return $url;
}, TRUE);

/**
 * Setting up the view component
 */
$di->set('view', function () use ($config) {

    $view = new View();

    $view->setViewsDir($config->application->viewsDirAdmin);

    $view->registerEngines([
        '.volt' => function ($view, $di) use ($config) {
            /** @var FactoryDefault $di  */
            $volt = new VoltEngine($view, $di);

            $volt->setOptions([
                'compiledPath' => $config->application->cacheDir,
                'compiledSeparator' => '_',
                'compileAlways' => $config->application->alwaysRecompileView,
            ]);

            $compiler = $volt->getCompiler();
            $compiler->addFunction('form_field', function($resolvedArgs) {
                return '$this->tag->form_field(' . $resolvedArgs . ')';
            });
            $compiler->addFunction('form_field_attributes', function($resolvedArgs) {
                return '$this->tag->form_field_attributes(' . $resolvedArgs . ')';
            });

            return $volt;
        },
        '.phtml' => 'Phalcon\Mvc\View\Engine\Php'
    ]);

    return $view;
}, TRUE);

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->set('db', function () use ($config) {
    $db_config = $config->toArray()['database'];

    return new DbAdapter($db_config);
});

$di->set('Acl', function () use ($config) {
    $cache_dir = $config->application->cacheDir . 'security';
    $cache_file = $cache_dir . '/acl.data';
    if (file_exists($cache_file)) {
        // Восстанавливаем ACL объект из текстового файла

        return unserialize(file_get_contents($cache_file));
    }

    $acl = new \Phalcon\Acl\Adapter\Memory();
    $acl->setDefaultAction(Phalcon\Acl::DENY);

    //... Определяем роли, ресурсы, доступ и т.д.
    $roleAdmin = new \Phalcon\Acl\Role('Admin');
    $roleUser = new \Phalcon\Acl\Role('User');
    $roleGuest = new \Phalcon\Acl\Role('Guest');

    $acl->addRole($roleGuest);
    $acl->addRole($roleUser, $roleGuest);
    $acl->addRole($roleAdmin, $roleUser);

    /*
     * Наследование идёт по ниспадающей, то есть
     * всё что можно гостю, можно пользователю,
     * а всё, что можно пользоваталю - админу
     */
    $resources = [
        'Guest' => [
            'index' => ['index'],
            'session' => ['start'],
            'add' => ['user', 'feedback'],
            'calculate' => ['path'],
            'get' => ['push', 'client_id'],
            'set' => ['push'],
            'send' => ['check_sms'],
            'error' => ['show404'],
        ],
        'User' => [
            'session' => ['end'],
        ],
    ];
    foreach ($resources as $role => $res) {
        foreach ($res as $resource => $actions) {
            $acl->addResource($resource, $actions);
            $acl->allow($role, $resource, $actions);
        }
    }
    $acl->allow('Admin', '*', '*');
    $acl->deny('Admin', 'session', 'start');

    // Сохраняем сериализованный объект в файл
    if (!file_exists($cache_dir)) {
        mkdir($cache_dir);
    }
    file_put_contents($cache_file, serialize($acl));

    return $acl;
});

/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->set('modelsMetadata', function () {
    return new MetaDataAdapter();
});

/**
 * Start the session the first time some component request the session service
 */
$di->set('session', function () {
    $session = new SessionAdapter();
    $session->start();

    return $session;
});

$di->set('filter', function () {
    $filter = new PhFilter();
    $filter->add('point', function ($value) {
        $matches = [];
        preg_match_all('/([\d]+\.[\d]+)/', $value, $matches);
        if (!isset($matches[0][0]) || !isset($matches[0][1])) {
            $value = [];
        } else {
            $value = [floatval($matches[0][0]), floatval($matches[0][1])];
            $value['latitude'] = $value[0];
            $value['longitude'] = $value[1];
        }

        return $value;
    });

    return $filter;
});

$di->set('security_plugin', function () use ($di) {

    return new Security($di);
}, TRUE);

$di->set('dispatcher', function () use ($di) {

    $eventsManager = $di->getShared('eventsManager');

    $eventsManager->attach(
        "dispatch:beforeException",
        function ($event, $dispatcher, $exception) {
            /** @var \Exception $exception */
            /** @var PhDispatcher $dispatcher */

            switch ($exception->getCode()) {
                case PhDispatcher::EXCEPTION_HANDLER_NOT_FOUND:
                case PhDispatcher::EXCEPTION_ACTION_NOT_FOUND:
                    $dispatcher->forward(
                        [
                            'controller' => 'index',
                            'action' => 'index',
                        ]
                    );

                    return FALSE;
            }

            return TRUE;
        }
    );

    //Instantiate the Security plugin
    $security = $di->get('security_plugin');

    //Listen for events produced in the dispatcher using the Security plugin
    $eventsManager->attach('dispatch', $security);

    $dispatcher = new PhDispatcher();
    $dispatcher->setEventsManager($eventsManager);

    return $dispatcher;
},
    TRUE
);

$di->set('http_curl', 'Libs\Http\Curl');

$di->set('maps_router', function (array $cities) use ($di) {
    return new LMaps\Google($di->get('http_curl'), $cities);
});

$di->set('integration_router', function () use ($di) {
    return new LIntegration\Integration($di->get('http_curl'));
});