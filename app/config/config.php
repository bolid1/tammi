<?php
if (!defined('DEV_ENV')) {
    define('DEV_ENV', (strpos(__DIR__, 'C:\WAMP') === 0));
}
if (DEV_ENV) {
    return include __DIR__ . '/config_dev.php';
}

return new \Phalcon\Config([
    'database' => [
        'adapter' => 'Mysql',
        'host' => 'localhost',
        'username' => 'maps',
        'password' => 'secret',
        'dbname' => 'tammi',
        'charset' => 'utf8',
    ],
    'application' => [
        'configDir' => __DIR__ . '/../../app/config/',
        'controllersDir' => __DIR__ . '/../../app/controllers/',
        'modelsDir' => __DIR__ . '/../../app/models/',
        'migrationsDir' => __DIR__ . '/../../app/migrations/',
        'viewsDirAdmin' => __DIR__ . '/../../app/views/admin/',
        'viewsDir' => __DIR__ . '/../../app/views/main/',
        'pluginsDir' => __DIR__ . '/../../app/plugins/',
        'libraryDir' => __DIR__ . '/../../app/library/',
        'cacheDir' => __DIR__ . '/../../app/cache/',
        'langDir' => __DIR__ . '/../../app/lang/',
        'baseUri' => '/',
        'alwaysRecompileView' => FALSE,
    ]
]);
