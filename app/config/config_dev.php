<?php

return new \Phalcon\Config([
    'database' => [
        'adapter' => 'Mysql',
        'host' => '127.0.0.1',
        'username' => 'maps',
        'password' => 'secret',
        'dbname' => 'maps',
        'charset' => 'utf8',
    ],
    'application' => [
        'configDir' => __DIR__ . '/../../app/config/',
        'controllersDir' => __DIR__ . '/../../app/controllers/',
        'modelsDir' => __DIR__ . '/../../app/models/',
        'migrationsDir' => __DIR__ . '/../../app/migrations/',
        'viewsDirAdmin' => __DIR__ . '/../../app/views/admin/',
        'viewsDir' => __DIR__ . '/../../app/views/main/',
        'pluginsDir' => __DIR__ . '/../../app/plugins/',
        'libraryDir' => __DIR__ . '/../../app/library/',
        'cacheDir' => __DIR__ . '/../../app/cache/',
        'langDir' => __DIR__ . '/../../app/lang/',
        'baseUri' => '/maps2/',
        'alwaysRecompileView' => TRUE,
    ]
]);
