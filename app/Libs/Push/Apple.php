<?php
namespace Libs\Push;

use Phalcon\Config;

class Apple implements Interfaces\Base {
	protected $_connection;

	function __construct(Config $config, $environment = 'dev') {
		$environment = $environment === 'prod' ? $environment : 'dev';

		$ctx = stream_context_create();
		$cert_path = $config->application->configDir . 'certs/apple/' . $environment . '.pem';
		stream_context_set_option($ctx, 'ssl', 'local_cert', $cert_path);
		stream_context_set_option($ctx, 'ssl', 'passphrase', '1234');

		// Open a connection to the APNS server
		$url = 'ssl://gateway';
		if ($environment === 'dev') {
			$url .= '.sandbox';
		}
		$url .= '.push.apple.com:2195';
		$this->_connection = stream_socket_client($url, $error_code, $error, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
		if (!$this->_connection) {
			throw new \RuntimeException('Connection failed: ' . $error_code . ': ' . $error, 500);
		}
	}

	function __destruct() {
		if ($this->_connection) {
			fclose($this->_connection);
		}
	}

	/**
	 * @inheritDoc
	 */
	public function send($token, $data) {
		if (empty($token) || empty($data)) {
			return FALSE;
		}

		if (!is_array($data)) {
			$data = $this->fill_data($data);
		}

		// Encode the payload as JSON
		$payload = json_encode($data);

		// Build the binary notification
		$notification = chr(0) . pack('n', 32) . pack('H*', $token) . pack('n', strlen($payload)) . $payload;

		// Send it to the server
		$result = fwrite($this->_connection, $notification, strlen($notification));

		return !empty($result);
	}

	protected function fill_data($message) {
		$body = [
			'aps' => [
				'alert' => $message,
				'sound' => 'default'
			]
		];

		return $body;
	}
}
