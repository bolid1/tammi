<?php
namespace Libs\Push\Interfaces;


interface Base {
    /**
     * @param $token
     * @param $data
     * @return bool
     */
    public function send($token, $data);
}