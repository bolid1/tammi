<?php
namespace Libs\Maps\Interfaces;


interface Map {
    /**
     * @param array $from
     * @param array $to
     * @return array
     * @throws \Libs\Maps\Exception
     */
    public function calculate_route_info(array $from, array $to);
}