<?php
namespace Libs\Maps;


use Libs\Http\Curl;

class Google implements Interfaces\Map {
    /** @var Curl */
    protected $_curl;

    /** @var array */
    protected $_cities;

    protected $_base_url = 'http://maps.googleapis.com/maps/api';

    function __construct(Curl $curl, array $cities = []) {
        $this->_curl = $curl;
        $this->_cities = $cities;
    }

    /**
     * @param array $from
     * @param array $to
     * @return array
     * @throws Exception
     */
    public function calculate_route_info(array $from, array $to) {
        if (!$this->point_valid($from) || !$this->point_valid($to)) {
            throw new Exception('Empty point value', 400);
        }

        if (!$city_id = $this->get_point_city($from)) {
            throw new Exception('Incorrect city', 400);
        }

        if (!$this->point_city_valid($to, $city_id)) {
            throw new Exception('Different city in points', 400);
        }

        $route_info = $this->get_route_info($from, $to);

        return [
            'city_id' => $city_id,
            'distance' => $route_info['distance']['value'],
            'duration' => $route_info['duration']['value'],
        ];
    }

    protected function point_valid(array $point) {
        for ($i = 0; $i < 2; ++$i) {
            if (empty($point[$i])) {
                return FALSE;
            }
        }

        return TRUE;
    }

    /**
     * @param array $point
     * @return bool|int
     */
    protected function get_point_city(array $point) {
        $possible_places = $this->get_possible_places($point);
        foreach ($possible_places as $place) {

            foreach ($this->_cities as $city_id => $city_title) {
                if (strpos($place['formatted_address'], $city_title) !== FALSE) {
                    return $city_id;
                }
            }
        }

        return FALSE;
    }

    /**
     * @param array $point
     * @param int $city_id
     * @return bool
     */
    protected function point_city_valid(array $point, $city_id) {
        $city_title = $this->_cities[$city_id];
        $possible_places = $this->get_possible_places($point);
        foreach ($possible_places as $place) {
            foreach ($place['address_components'] as $address_component) {
                if (strpos($address_component['long_name'], $city_title) !== FALSE) {
                    return TRUE;
                }
            }
        }

        return FALSE;
    }

    protected function wrap_point_for_uri($point) {
        foreach (['latitude', 'longitude'] as $key) {
            $point[$key] = str_replace(',', '.', $point[$key]);
        }

        return "{$point['latitude']},{$point['longitude']}";
    }

    /**
     * @param array $point
     * @return array
     */
    protected function get_possible_places(array $point) {
        $curl_result = $this->_curl->get($this->_base_url . '/geocode/json', [
            'sensor' => 'false',
            'language' => 'ru',
            'latlng' => $this->wrap_point_for_uri($point),
        ]);
        $curl_result = json_decode($curl_result, TRUE);

        return $curl_result['results'];
    }

    protected function get_route_info($origin, $destination) {
        $curl_result = $this->_curl->get($this->_base_url . '/directions/json', [
            'sensor' => 'false',
            'destination' => $this->wrap_point_for_uri($destination),
            'origin' => $this->wrap_point_for_uri($origin),
        ]);
        $curl_result = json_decode($curl_result, TRUE);
        if ($curl_result['status'] != 'OK') {
            throw new Exception('Google return status ' . $curl_result['status'], 400);
        }

        $route = $curl_result['routes'][0];

        return $route['legs'][0];
    }
}