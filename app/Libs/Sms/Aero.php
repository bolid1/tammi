<?php
namespace Libs\Sms;


use Libs\Http\Curl;

class Aero implements Interfaces\Base {
    /** @var Curl */
    protected $_curl;

    protected $_from;

    protected $_credentials;

    protected $_base_url = 'https://gate.smsaero.ru/';

    /**
     * @inheritDoc
     */
    function __construct(Curl $curl) {
        $this->_curl = $curl;
        $this->_credentials = [
            'user' => 'VDVUGaD@gmail.com',
            'password' => md5(123456)
        ];

        $this->check_balance();
        $this->_from = $this->get_from();
    }

    /**
     * @inheritDoc
     */
    public function send($number, $text) {
        $url = $this->_base_url . "send/";
        $post_data = [
                'to' => $number,
                'text' => $text,
                'from' => $this->_from,
            ] + $this->_credentials;

        $result = $this->_curl->post($url, [], $post_data);
        $result = explode('=', $result, 2);
        // @TODO: $result[0] - message id log it!

        return $result[1] == 'accepted';
    }

    protected function get_from() {
        $url = $this->_base_url . 'senders/';
        $names_string = $this->_curl->post($url, [], $this->_credentials);
        $names = [];
        preg_match_all('"(\w+)"', $names_string, $names);
        if (in_array('tammi', array_map('strtolower', $names[0]), TRUE)) {
            $result = 'Tammi';
        } else {
            $result = end($names);
        }

        return $result;
    }

    protected function check_balance() {
        $url = $this->_base_url . 'balance/';
        $balance = $this->_curl->post($url, ['answer' => 'json'], $this->_credentials);
        $balance = json_decode($balance, TRUE);
        if (floatval($balance['balance']) < 100) {
            // @TODO: send notification
        }
    }
}