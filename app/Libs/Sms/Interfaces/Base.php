<?php
namespace Libs\Sms\Interfaces;


interface Base {
    /**
     * @param string $number
     * @param string $text
     * @return bool
     */
    public function send($number, $text);
}