<?php
namespace Libs\Integration\RelationManager\Interfaces;

use Libs\Integration\RelationManager\Exception;
use Tariff;

interface Price {
    /**
     * Try to get price from company API
     *
     * @param array $start_point
     * @param array $end_point
     * @param Tariff $tariff
     * @return double
     * @throws Exception
     */
    public function get_price(array $start_point, array $end_point, $tariff);
}