<?php
namespace Libs\Integration\RelationManager;


use Libs\Http\Curl;
use Tariff;

class Uber extends Base implements Interfaces\Base, Interfaces\Price {
    protected $_server_token = 'DjCwAOv4CRXyIIWmPpXJr11RV-LtuUpPziixWNSF';

    function __construct(Curl $curl) {
        parent::__construct($curl);
        $this->_base_uri = DEV_ENV ? 'https://sandbox-api.uber.com/v1' : 'https://api.uber.com/v1';
    }

    /**
     * Try to get price from company API
     *
     * @param array $start_point
     * @param array $end_point
     * @param Tariff $tariff
     * @return double
     * @throws Exception
     */
    public function get_price(array $start_point, array $end_point, $tariff) {
        $curl_result = $this->_curl->get($this->_base_uri . '/estimates/price', [
            'server_token' => $this->_server_token,
            'start_latitude' => $start_point['latitude'],
            'start_longitude' => $start_point['longitude'],
            'end_latitude' => $end_point['latitude'],
            'end_longitude' => $end_point['longitude'],
        ]);
        $curl_result = json_decode($curl_result, TRUE);

        $tariff_title = strtolower(trim($tariff->title));
        $result = FALSE;
        foreach ($curl_result['prices'] as $price) {
            if (strtolower(trim($price['display_name'])) == $tariff_title) {
                foreach (['low_estimate', 'high_estimate'] as $key) {
                    if (empty($price[$key])) {
                        $price[$key] = 0;
                    } else {
                        $price[$key] = doubleval($price[$key]);
                    }
                }

                $result = ($price['low_estimate'] + $price['high_estimate']) / 2;

                break;
            }
        }

        if (!$result) {
            throw new Exception('Can\'t find price for tariff ' . $tariff->title, 400);
        }

        return $result;
    }

    /**
     * Пока не используется. Был написан в процессе освоения их API
     *
     * @param array $start_point
     * @param $tariff
     * @return array
     */
    protected function get_product(array $start_point, $tariff) {
        $curl_result = $this->_curl->get($this->_base_uri . '/products', [
            'server_token' => $this->_server_token,
            'latitude' => $start_point['latitude'],
            'longitude' => $start_point['longitude'],
        ]);
        $curl_result = json_decode($curl_result, TRUE);

        foreach ($curl_result['products'] as $product) {
            if ($product['display_name'] == $tariff->title) {
                return $product;
            }
        }

        return FALSE;
    }
}