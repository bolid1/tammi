<?php
namespace Libs\Integration\RelationManager;

use Libs\Http\Curl;

class Base implements Interfaces\Base {
    /** @var  Curl */
    protected $_curl;

    protected $_base_uri = 'https://api.tammi.com';

    function __construct(Curl $curl) {
        $this->_curl = $curl;
    }
}