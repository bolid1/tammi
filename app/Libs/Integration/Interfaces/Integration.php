<?php
namespace Libs\Integration\Interfaces;

use Libs\Integration\Exception;
use Tariff;


interface Integration {
    /**
     * Try to find company relation manager object
     *
     * @param $code
     * @return bool true on success and false otherwise
     */
    public function set_company($code);

    /**
     * Try to get price from company API
     *
     * @param array $start_point
     * @param array $end_point
     * @param Tariff $tariff
     * @return double
     * @throws Exception
     */
    public function get_price(array $start_point, array $end_point, Tariff $tariff);
}