<?php
namespace Libs\Integration;

use Libs\Http\Curl;
use Tariff;

class Integration implements Interfaces\Integration {
    /** @var Curl */
    protected $_curl;

    /** @var RelationManager\Interfaces\Base */
    protected $_relation_manager = NULL;

    function __construct(Curl $curl) {
        $this->_curl = $curl;
    }

    /**
     * Try to get price from company API
     *
     * @param array $start_point
     * @param array $end_point
     * @param Tariff $tariff
     * @return double
     * @throws Exception
     */
    public function get_price(array $start_point, array $end_point, Tariff $tariff) {
        if (is_null($this->_relation_manager)) {
            throw new Exception('Relation Manager not defined', 500);
        }

        if (!method_exists($this->_relation_manager, 'get_price')) {
            throw new Exception('There is not method to get price in Relation Manager');
        }

        /** @var RelationManager\Interfaces\Price $relation_manager */
        $relation_manager = $this->_relation_manager;

        try {
            $price = $relation_manager->get_price($start_point, $end_point, $tariff);
        } catch (Exception $ex) {
            throw new Exception('Relation Manager can\'t get price', 500, $ex);
        }

        return $price;
    }


    /**
     * Try to find company relation manager object
     *
     * @param $code
     * @return bool true on success and false otherwise
     */
    public function set_company($code) {
        $relation_manager = $this->get_relation_manager($code);
        if ($relation_manager) {
            $this->_relation_manager = $relation_manager;
        }

        return !empty($relation_manager);
    }

    /**
     * @param $company_code
     * @return bool|RelationManager\Interfaces\Base
     */
    protected function get_relation_manager($company_code) {
        $result = FALSE;
        switch ($company_code) {
            case 'uber':
                $result = new RelationManager\Uber($this->_curl);
                break;
        }

        return $result;
    }
}