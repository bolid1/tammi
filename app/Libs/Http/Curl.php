<?php
namespace Libs\Http;


class Curl {
    protected $_curl;

    protected $_options = [
        CURLOPT_RETURNTRANSFER => TRUE,
        CURLOPT_AUTOREFERER => TRUE,
        CURLOPT_FOLLOWLOCATION => TRUE,
        CURLOPT_CONNECTTIMEOUT => 30,
        CURLOPT_SSL_VERIFYPEER => FALSE,
    ];

    function __construct() {
        $this->init();
    }

    function __destruct() {
        $this->close();
    }

    /**
     * @param array $options
     * @param bool $reload
     * @return $this
     * @throws Exception
     */
    public function init(array $options = [], $reload = FALSE) {
        if ($reload) {
            $this->close();
        }

        if ($this->need_init()) {
            $this->_curl = curl_init();
        }

        $this->set_options($options + $this->_options);

        return $this;
    }

    protected function need_init() {
        return is_null($this->_curl);
    }

    protected function close() {
        if (!$this->need_init()) {
            curl_close($this->_curl);
        }

        return $this;
    }

    /**
     * @return array|bool
     * @throws Exception
     */
    protected function execute() {
        if ($this->need_init()) {
            throw new Exception('Curl not initialized', 500);
        }

        return curl_exec($this->_curl);
    }

    public function get_last_error() {
        return ['number' => curl_errno($this->_curl), 'message' => curl_error($this->_curl)];
    }

    /**
     * Set options to current curl object
     *
     * @param array $options
     * @return $this
     * @throws Exception
     */
    protected function set_options(array $options) {
        if ($this->need_init()) {
            throw new Exception('Curl not initialized', 500);
        }

        curl_setopt_array($this->_curl, $options);

        return $this;
    }

    /**
     * @param string $url
     * @param array $url_params
     * @param array $options
     * @return array|bool
     * @throws Exception
     */
    public function get($url, array $url_params = [], array $options = []) {
        $options += $this->_options + [
                CURLOPT_POST => FALSE,
                CURLOPT_HTTPGET => TRUE,
                CURLOPT_URL => $this->build_url($url, $url_params),
            ];

        return $this->set_options($options)->execute();
    }

    /**
     * @param string $url
     * @param array $url_params
     * @param array $post_data
     * @param array $options
     * @return array|bool
     * @throws Exception
     */
    public function post($url, array $url_params = [], array $post_data = [], array $options = []) {
        $options += $this->_options + [
                CURLOPT_POST => TRUE,
                CURLOPT_URL => $this->build_url($url, $url_params),
                CURLOPT_POSTFIELDS => $post_data,
            ];

        return $this->set_options($options)->execute();
    }

    /**
     * @param string $url
     * @param array $url_params
     * @return string
     */
    protected function build_url($url, array $url_params) {
        $url = trim($url, '?');

        $query_string = http_build_query($url_params);
        if (!empty($query_string)) {
            $url .= '?' . $query_string;
        }

        return $url;
    }
}