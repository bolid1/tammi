<?php

use Phalcon\Mvc\Controller;

/** @property \Plugins\Tag $tag */
class ControllerBase extends Controller {
    /** @var  \Phalcon\Translate\Adapter\NativeArray */
    protected $_lang;

    private function _initTranslation() {
        $lang_directory = $this->di->get('config')->application->langDir;
        // Получение оптимального языка из браузера
        $language = $this->request->getBestLanguage();
        $language = strtok($language, '-');
        // Проверка существования перевода для полученного языка
        if (!file_exists("{$lang_directory}{$language}.php")) {
            $language = 'en';
        }

        $messages = include "{$lang_directory}{$language}.php";

        // Возвращение объекта работы с переводом
        $this->_lang = new \Phalcon\Translate\Adapter\NativeArray([
            'content' => $messages
        ]);
    }

    protected function get_json_response($value, $status = 200) {
        // Получение экземпляра Response
        $response = new \Phalcon\Http\Response();
        $response->setHeader('Content-Type', 'application/json');

        // Установка содержимого ответа
        $response->setJsonContent($value);

        if ($status != 200) {
            $response->setStatusCode($status);
        }

        // Возврат Response ответа
        return $response;
    }

    public function initialize() {
        $this->_initTranslation();

        $this->view->setVar('user', $this->get_user());
        $this->view->setVar('security_plugin', $this->di->get('security_plugin'));
        $this->view->setVar('lang', $this->_lang);
    }

    public function afterExecuteRoute(){
        $this->view->setVar('template', $this->get_base_template_params());
    }

    private function get_user($key = NULL) {
        // Authorization staff
        if ($auth = $this->session->get('auth')) {
            $user = [
                'email' => $auth['email'],
                'authorized' => TRUE,
            ];
        } else {
            $user = [
                'email' => 'Guest',
                'authorized' => FALSE,
            ];
        }

        if (!is_null($key)) {
            return (isset($user[$key]) ? $user[$key] : NULL);
        }

        return $user;
    }

    private function get_base_template_params() {
        $menu = $this->get_base_template_menu();

        return [
            'security' => [
                'token' => [
                    'key' => $this->security->getTokenKey(),
                    'value' => $this->security->getToken(),
                ],
            ],
            'menu' => $menu,
            'java_scripts' => [],
        ];
    }

    private function get_base_template_menu() {
        /** @var \Plugins\Security $security */
        $security = $this->di->get('security_plugin');
        $entities = ['city', 'company', 'tariff', 'user', 'users'];
        $actions = ['index', 'new', 'search'];

        $menu = [];
        foreach ($entities as $entity) {
            foreach ($actions as $action) {
                if (!$security->has_access($entity, $action)) {
                    continue;
                }
                if (!isset($menu[$entity])) {
                    $menu[$entity] = [
                        'type' => 'drop_down',
                        'text' => $this->_lang[$entity],
                        'items' => [],
                    ];
                }

                $menu[$entity]['items'][$action] = [
                    'divider' => $action === 'search',
                    'text' => $this->_lang->_(ucfirst($action)),
                    'href' => "{$entity}/{$action}",
                ];
            }
        }

        $menu['user']['items']['push_all'] = [
            'divider' => TRUE,
            'text' => $this->_lang->_('Push'),
            'href' => "user/push_all",
        ];

        $menu['stats'] = [
            'text' => $this->_lang->_('Stats'),
            'href' => '#',
        ];

        if ($security->has_access('session', 'end')) {
            $menu['logout'] = [
                'text' => $this->get_user('email'),
                'href' => 'session/end',
            ];
        }

        $menu = [
            'items' => $menu,
        ];

        return $menu;
    }
}
