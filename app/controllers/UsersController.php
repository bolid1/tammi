<?php

use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class UsersController extends ControllerModel {
    public function initialize() {
        $this->_model = new Users();
        parent::initialize();

        unset($this->_table_fields['password']);
    }

    protected function prepare_data_from_request($data) {
        if (isset($data['password'])) {
            $data['password'] = $this->security->hash($data['password']);
        }

        return $data;
    }

}
