<?php

class setController extends ControllerBase {
    public function pushAction() {
        $user_id = $this->request->getQuery('user_id', 'int', '0');
        $push = $this->request->getQuery('push_token', 'string', '');

        if (empty($push)) {
            $result = ['error' => 'invalid_data'];

            return $this->get_json_response($result, 400);
        }

        if ($user_id <= 0 || !($user = User::findFirst($user_id))) {
            $result = ['error' => 'invalid_user'];

            return $this->get_json_response($result, 400);
        }

        /** @var User $user */
        $user->push_token = $push;
        $user->save();

        $result = ['success' => 1];

        return $this->get_json_response($result);
    }
}

