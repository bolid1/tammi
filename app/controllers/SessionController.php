<?php

class SessionController extends ControllerBase {
    public function indexAction() {

    }

    public function startAction() {
        if (!$this->request->isPost()) {
            $this->flash->error('Wrong request method');

            return;
        }

        $token = [
            'key' => $this->request->getPost('token_key'),
            'value' => $this->request->getPost('token_value'),
        ];
        if (FALSE && !$this->security->checkToken($token['key'], $token['value'])) {
            $this->flash->error("Wrong token");

            return;
        }

        //Receiving the variables sent by POST
        $email = $this->request->getPost('email', 'email');
        $password = $this->request->getPost('password');

        //Find for the user in the database
        $user = Users::findFirst([
            'email = :email:',
            'bind' => ['email' => strtolower($email)]
        ]);
        if ($user != FALSE) {
            /** @var Users $user */
            if ($this->security->checkHash($password, $user->password)) {
                $this->_registerSession($user);

                $this->flash->success('Welcome ' . $user->email);

                $this->dispatcher->forward([
                    'controller' => 'index',
                    'action' => 'index'
                ]);

                return;
            }
        }

        $this->flash->error('Wrong email/password');
    }

    public function endAction() {
        if ($this->session->isStarted()) {
            $this->session->destroy();
            $this->session->remove('auth');
            $this->initialize();
        }
    }

    protected function _registerSession(Users $user) {
        $this->session->set('auth', [
            'id' => $user->id,
            'email' => $user->email,
            'role' => $user->role,
        ]);
    }
}

