<?php

class GetController extends ControllerBase {
    public function pushAction() {
        $user_id = $this->request->getQuery('user_id', 'int', '0');

        if ($user_id <= 0 || !($user = User::findFirst($user_id))) {
            $result = ['error' => 'invalid_user'];

            return $this->get_json_response($result, 400);
        }

        /** @var User $user */

        $result = [
            'user_id' => $user->id,
            'push_token' => $user->push_token,
        ];

        return $this->get_json_response($result);
    }

    public function client_idAction() {
        $user_id = $this->request->getQuery('user_id', 'int', '0');
        $to = $this->request->getQuery('phone');
        $in_code = (int)$this->request->getQuery('code', 'int');

        if ($user_id <= 0 || !($user = User::findFirst($user_id))) {
            $result = ['error' => 'invalid_user'];

            return $this->get_json_response($result, 400);
        }
        $code = Client::generateCode($user_id, $to);

        if ($in_code !== $code) {
            $result = ['error' => 'invalid_code'];

            return $this->get_json_response($result, 400);
        }

        /** @var Client $client */
        $client = Client::findFirst([
            'phone = :phone:',
            'bind' => ['phone' => $to],
        ]);
        if (empty($client)) {
            $client = new Client();
            $client->save(['phone' => $to]);
        }

        /** @var UserClientRelation $relation */
        $relation = UserClientRelation::findFirst([
            'user_id = :user_id: AND client_id = :client_id:',
            'bind' => ['user_id' => $user_id, 'client_id' => $client->id],
        ]);

        if (empty($relation)) {
            $relation = new UserClientRelation();
            $relation->save(['user_id' => $user_id, 'client_id' => $client->id]);
        }

        /** @var User $user */
        $result = [
            'user_ids' => $user->id,
            'client_id' => $client->id,
            'relation_id' => $relation->id,
        ];

        return $this->get_json_response($result);
    }
}

