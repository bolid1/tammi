<?php

class AddController extends ControllerBase {
    public function userAction() {
        $os = $this->request->getQuery('os');

        $user = new User();
        $os_list = $user->get_os_list();

        if (!in_array($os, $os_list)) {
            $result = ['error' => 'invalid_data', 'os_list' => $os_list];

            return $this->get_json_response($result, 400);
        }

        $os_list = array_flip($os_list);

        $user->create(['os' => $os_list[$os]]);
        $result = ['user_id' => $user->id];

        return $this->get_json_response($result);
    }

    public function feedbackAction() {
        if ($this->request->isAjax() && $this->request->isPost()) {
            $response = ['status' => 'fail'];
            $email = $this->request->getPost('email');
            if (!empty($email) && filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $email = strtolower($email);
                $model = new LandingMails();
                if (!$model->findFirst(['email=:email:', 'bind' => ['email' => $email]])) {
                    $model->email = $email;
                    $model->send();
                    $response['status'] = 'success';
                } else {
                    $response['status'] = 'exist';
                }
            } else {
                $response['status'] = 'invalid';
            }

            return $this->get_json_response($response);
        }


        $this->dispatcher->forward([
            'action' => 'index',
        ]);

        return null;
    }
}

