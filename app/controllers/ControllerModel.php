<?php
use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as PageModel;

abstract class ControllerModel extends ControllerBase {
    /** @var ModelBase */
    protected $_model;
    protected $_model_per_page = 10;
    protected $_cache_keys;
    protected $_table_fields;
    protected $_last_saved_id = NULL;
    protected $_form_fields = [];

    public function initialize() {
        parent::initialize();
        $this->_cache_keys = [
            'index' => __CLASS__ . 'index_parameters',
        ];
        $this->_model->initialize();

        $fields = array_keys($this->_model->asArray());
        $this->_table_fields = [];
        foreach ($fields as $field) {
            $this->_table_fields[$field] = $this->_lang->_($field);

            $form_field = [
                'control-type' => 'textField',
                'class' => 'form-control',
                'id' => $field,
                'placeholder' => ($this->_lang->_($field)),
            ];

            if ($field === 'id') {
                $form_field['disabled'] = 'disabled';
            } elseif ($field === 'description') {
                $form_field = [
                        'control-type' => 'textArea',
                        'rows' => 5,
                    ] + $form_field;
            }

            $this->_form_fields[$field] = $form_field;
        }

        $this->view->setVar('entity', $this->dispatcher->getControllerName());
    }

    /**
     * List of models
     */
    public function indexAction() {
        $parameters_key = $this->_cache_keys['index'];
        $criteria = new Criteria();
        if ($this->request->isPost()) {
            $query = $criteria->fromInput($this->di, get_class($this->_model), $this->request->getPost());
            $this->persistent->set($parameters_key, $query->getParams());
        }

        $parameters = $this->persistent->get($parameters_key);
        if (empty($parameters)) {
            $parameters['order'] = 'id';
        }

        $pagination = new PageModel([
            'data' => $this->_model->find($parameters),
            'limit' => $this->_model_per_page,
            'page' => $this->request->getQuery('page', 'int')
        ]);

        $this->view->setVar('fields', $this->_table_fields);
        $this->view->setVar('data', $pagination->getPaginate());
    }

    /**
     * Set search params
     */
    public function searchAction() {
        $parameters_key = $this->_cache_keys['index'];
        $this->persistent->set($parameters_key, NULL);

        $model_array = $this->_model->asArray();

        $this->tag->set_defaults($this->_form_fields);
        $this->view->setVar('fields', array_keys($model_array));
        $this->view->setVar('action', 'index');
    }

    /**
     * Displays the creation form
     */
    public function newAction() {
        $model_array = $this->_model->asArray();

        $this->_form_fields['id']['control-type'] = 'hiddenField';
        $this->tag->set_defaults($this->_form_fields);
        $this->view->setVar('fields', array_keys($model_array));
        $this->view->setVar('action', 'create');
        $this->tag->setDefaults($model_array);
    }

    /**
     * Display edit form
     *
     * @param int $id
     */
    public function editAction($id = 0) {
        /** @var ModelBase $model */
        $model = $this->_model->findFirstById($id);
        if ($model === FALSE) {
            $this->flash->error('Wrong model id');

            $this->dispatcher->forward([
                'action' => 'index'
            ]);
            return;
        }

        $model_array = $model->asArray();

        $this->tag->set_defaults($this->_form_fields);
        $this->view->setVar('fields', array_keys($model_array));
        $this->view->setVar('action', 'save/' . $id);
        $this->tag->setDefaults($model_array);
    }

    /**
     * Create new model from POST request
     */
    public function createAction() {
        if (!$this->request->isPost()) {
            $this->flash->error('Wrong request type');
            $this->dispatcher->forward([
                'action' => 'index'
            ]);
            return;
        }

        $model = clone($this->_model);
        $this->_saveFromRequest($model);
    }

    /**
     * Save model data from POST request
     * @param int $id
     */
    public function saveAction($id = 0) {
        if (!$this->request->isPost()) {
            $this->flash->error('Wrong request type');
            $this->dispatcher->forward([
                'action' => 'index'
            ]);
            return;
        }

        $model = $this->_model->findFirstById($id);
        if (!$model) {
            $this->flash->error('Wrong model id');
            $this->dispatcher->forward([
                'action' => 'index'
            ]);
            return;
        }

        $this->_saveFromRequest($model);
    }

    /**
     * Delete model
     * @param int $id
     */
    public function deleteAction($id = 0) {
        $model = $this->_model->findFirstById($id);
        if (!$model) {
            $this->flash->error('Wrong model id');
            $this->dispatcher->forward([
                'action' => 'index'
            ]);
            return;
        }

        if (!$model->delete()) {
            $messages = $model->getMessages();
            foreach ($messages as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'action' => 'new',
            ]);

            return;
        }

        $this->flash->success('Saved successfully');
        $this->dispatcher->forward([
            'action' => 'index'
        ]);
    }

    protected function _saveFromRequest(ModelBase $model, $data = [], $redirect_on_end = TRUE) {
        if (empty($data)) {
            $data = $this->request->getPost();
        }

        $data = $this->prepare_data_from_request($data);

        $save_result = $model->save($data);

        $action = $this->dispatcher->getActionName() == 'save' ? 'edit' : 'new';
        $this->_last_saved_id = $id = $model->getId();
        $dispatcher_params = [];
        if (!is_null($id)) {
            $dispatcher_params[] = $id;
        }

        if (!$save_result) {
            $messages = $model->getMessages();
            foreach ($messages as $message) {
                $this->flash->error($message);
            }

            $this->dispatcher->forward([
                'action' => $action,
                'params' => $dispatcher_params
            ]);
            return;
        }

        $this->flash->success('Saved successfully');
        if ($redirect_on_end) {
            $this->dispatcher->forward([
                'action' => 'edit',
                'params' => $dispatcher_params
            ]);
            return;
        }
    }

    protected function prepare_data_from_request($data) {
        return $data;
    }
}
