<?php

use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class CompanyController extends ControllerModel {
    public function initialize() {
        $this->_model = new Company();
        parent::initialize();

        foreach ($this->_table_fields as $field => $name) {
            if (!in_array($field, ['id', 'title', 'description', 'site_url'], TRUE)) {
                unset($this->_table_fields[$field]);
            }
        }
    }
}
