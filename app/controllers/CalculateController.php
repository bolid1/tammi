<?php
use \Libs\Maps as LMaps;
use \Libs\Integration as LIntegration;

class CalculateController extends ControllerBase {
    public function pathAction() {
        $start_point = $this->request->getQuery('start_point', 'point', [55.709495, 37.663629]);
        $end_point = $this->request->getQuery('end_point', 'point', [55.709495, 37.563629]);
        $user_id = $this->request->getQuery('user_id', 'int', '0');
        /** @var LIntegration\Interfaces\Integration $integration_router */
        $integration_router = $this->di->get('integration_router');

        if ($user_id <= 0 || User::count(["id=:id:", 'bind' => ['id' => $user_id]]) < 1) {
            $result = ['error' => 'invalid_user'];

            return $this->get_json_response($result, 400);
        }

        try {
            /** @var LMaps\Interfaces\Map $maps_router */
            $maps_router = $this->di->get('maps_router', [City::getList(NULL, 'title')]);
            $route_info = $maps_router->calculate_route_info($start_point, $end_point);
        } catch (LMaps\Exception $ex) {
            $result = ['error' => 'invalid_coordinates', 'message' => $ex->getMessage()];

            return $this->get_json_response($result, 400);
        }

        $result = [
            'route_info' => $route_info,
            'tariffs' => [],
            'companies' => [],
        ];

        $tariffs = Tariff::find(["city_id=:city_id:", 'bind' => ['city_id' => $route_info['city_id']]]);
        /** @var Tariff $tariff */
        foreach ($tariffs as $tariff) {
            $times = $tariff->get_current_times();
            /** @var TariffTime $time */
            if ($times->count() > 0) {
                $tariff_array = [
                    'tariff' => $tariff->asArray(NULL, TRUE),
                    'price' => $tariff->calculate($route_info['distance'], $route_info['duration']),
                ];


                $company_integration = $tariff->getCompany()->getIntegration();
                if ($company_integration && ($company_integration = $company_integration->asArray()) && $company_integration['calculation']) {
                    try {
                        if ($integration_router->set_company($company_integration['code'])) {
                            $tariff_array['price_backup'] = $tariff_array['price'];
                            $tariff_array['price'] = $integration_router->get_price($start_point, $end_point, $tariff);
                        }
                    } catch (LIntegration\Exception $ex) {
                    }
                }

                $result['tariffs'][] = $tariff_array;

                if (!isset($result['companies'][$tariff->company_id])) {
                    $company = $tariff->getCompany();
                    $result['companies'][$company->id] = $company->asArray(NULL, TRUE);
                }
            }
        }

        if (!empty($result['tariffs'])) {
            $stat = new Stats();

            usort($result['tariffs'], function ($tariff_1, $tariff_2) {
                return $tariff_1['price'] - $tariff_2['price'];
            });

            $tariffs_id = array_keys($result['tariffs']);
            reset($tariffs_id);
            $min_tariff_id = next($tariffs_id);
            $max_tariff_id = end($tariffs_id);

            $stat->create([
                'user_id' => $user_id,

                'start_point' => json_encode($start_point),
                'end_point' => json_encode($end_point),
                'distance' => $route_info['distance'],
                'time' => $route_info['duration'],

                'min_price' => $result['tariffs'][$min_tariff_id]['price'],
                'min_tariff_id' => $min_tariff_id,

                'max_price' => $result['tariffs'][$max_tariff_id]['price'],
                'max_tariff_id' => $max_tariff_id,
            ]);
        }

        return $this->get_json_response($result);
    }
}

