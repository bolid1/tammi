<?php

class IndexController extends ControllerBase {
    public function initialize() {
        parent::initialize();
        $this->view->setViewsDir($this->di->get('config')->application->viewsDir);
    }


    public function indexAction() {
        $this->view->setLayout('landing');
    }
}

