<?php

class sendController extends ControllerBase {
    public function check_smsAction() {
        $user_id = $this->request->getQuery('user_id', 'int', '0');
        $to = $this->request->getQuery('phone');

        $result = ['status' => 'fail'];

        if ($user_id > 0 && ($user = User::findFirst($user_id))) {
            if (!empty($to)) {
                $code = Client::generateCode($user_id, $to);
                $message = $this->_lang->_('sms-confirm-phone-number', ['code' => $code]);
                $sms = new \Libs\Sms\Aero($this->di->get('http_curl'));
                if ($sms->send($to, $message)) {
                    $result['status'] = 'success';
                }
            }
        } else {
            $result['error'] = 'invalid_user';
        }

        return $this->get_json_response($result);
    }
}

