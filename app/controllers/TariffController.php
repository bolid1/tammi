<?php

use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

/** @property Tariff $_model */
class TariffController extends ControllerModel {
    public function initialize() {
        $this->_model = new Tariff();
        parent::initialize();

        foreach ($this->_table_fields as $field => $name) {
            if (!in_array($field, ['id', 'company_id', 'city_id', 'title', 'description', 'class', 'min_price', 'init_price'], TRUE)) {
                unset($this->_table_fields[$field]);
            }
        }

        foreach (['type', 'class'] as $field) {
            $form_field = [
                'control-data' => $this->_model->getVariants($field),
                'control-type' => 'selectStatic',
            ];

            array_walk($form_field['control-data'], function (&$value, $key, $lang) {
                /** @var \Phalcon\Translate\Adapter\NativeArray $lang */
                $value = $lang->_($value);
            }, $this->_lang);
            $this->_form_fields[$field] = $form_field + $this->_form_fields[$field];
        }

        foreach (['company_id', 'city_id'] as $field) {
            $form_field = [
                'control-data' => $this->_model->getVariants($field),
                'control-type' => 'select',
                'using' => ['id', 'title'],
            ];

            $this->_form_fields[$field] = $form_field + $this->_form_fields[$field];
        }

        foreach (['min_price', 'init_price', 'init_time', 'init_distance', 'unit_time', 'price_time', 'unit_distance', 'price_distance'] as $field) {
            $form_field = [
                'control-type' => 'numericField',
            ];

            $this->_form_fields[$field] = $form_field + $this->_form_fields[$field];
        }
    }

    public function newAction() {
        parent::newAction();
        $tariff_time_model = new TariffTime();
        list ($form_fields, $base_name) = $this->get_tariff_time_template();
        $tariff_times = [$base_name];
        $this->tag->set_defaults($form_fields);

        $this->view->setVar('tariff_times', $tariff_times);
        $this->view->setVar('tariff_time_fields', array_keys($tariff_time_model->asArray()));

        $template = $this->view->getVar('template');
        $template['java_script'][] = 'tariff_time.add.js';
        $this->view->setVar('template', $template);
        $this->view->setVar('days', $this->get_active_day_variants());
    }

    protected function get_tariff_time_template($id = 'new') {
        $base_name = "tariff_time[{$id}]";
        $base_id = "tariff_time_{$id}";

        $fields = [];

        $field_name = $base_name . '[delete]';
        $field_id = $base_id . '_delete';
        $fields[$field_name] = [
            'control-type' => 'checkField',
            'class' => 'form-control js-delete-new',
            'name' => $field_name,
            'id' => $field_id,
            'placeholder' => ($this->_lang->_('delete')),
        ];

        $field_name = $base_name . '[active_day]';
        $field_id = $base_id . '_active_day';
        $fields[$field_name] = [
            'control-data' => $this->get_active_day_variants(),
            'control-type' => 'selectStatic',
            'class' => 'form-control',
            'name' => $field_name,
            'id' => $field_id,
            'placeholder' => ($this->_lang->_('active_day')),
        ];

        foreach (['active_time_from', 'active_time_till'] as $field) {
            $field_name = $base_name . '[' . $field . ']';
            $field_id = $base_id . '_' . $field;
            $fields[$field_name] = [
                'control-type' => 'timeField',
                'class' => 'form-control',
                'name' => $field_name,
                'id' => $field_id,
                'placeholder' => ($this->_lang->_($field)),
            ];
        }

        return [$fields, $base_name];
    }

    protected function get_active_day_variants() {
        static $variants = NULL;
        if (!is_null($variants)) {
            return $variants;
        }

        $tariff_time_model = new TariffTime();
        $tariff_time_model->initialize();
        $variants = $tariff_time_model->getVariants('active_day');

        array_walk($variants, function (&$value, $key, $lang) {
            /** @var \Phalcon\Translate\Adapter\NativeArray $lang */
            $value = $lang->_($value);
        }, $this->_lang);

        return $variants;
    }

    public function editAction($id = 0) {
        parent::editAction($id);
        /** @var Tariff $model */
        $model = $this->_model->findFirstById($id);
        $times = $model->getTimes();
        $tariff_time_model = new TariffTime();
        $tariff_times = [];
        $tariff_times_form_fields = [];

        if ($times->count() > 0) {
            foreach ($times as $time) {
                /** @var TariffTime $time */
                $time_array = $time->asArray();
                $time_array['delete'] = 0;

                $tariff_times[] = $base_name = "tariff_time[{$time_array['id']}]";
                foreach ($time_array as $field => $value) {
                    $id = "tariff_time_{$time_array['id']}_{$field}";
                    $name = "{$base_name}[{$field}]";
                    $form_field = [
                        'control-type' => 'textField',
                        'class' => 'form-control',
                        'name' => $name,
                        'id' => $id,
                        'placeholder' => ($this->_lang->_($field)),
                    ];

                    if ($field === 'id') {
                        $form_field['control-type'] = 'hiddenField';
                    } elseif ($field === 'tariff_id') {
                        // Нахер эту инфу
                        continue;
                    } elseif ($field == 'active_day') {
                        $form_field = [
                            'control-data' => $this->get_active_day_variants(),
                            'control-type' => 'selectStatic',
                        ] + $form_field;
                    } elseif (in_array($field, ['active_time_from', 'active_time_till'], TRUE)) {
                        $form_field['control-type'] = 'timeField';
                        $hours = floor($value / (60 * 60));
                        $minutes = floor(($value - $hours * 60 * 60) / 60);

                        $value = sprintf("%02d:%02d", $hours, $minutes);
                    } elseif ($field == 'delete') {
                        $form_field['control-type'] = 'checkField';
                        $form_field['class'] .= ' js-delete';
                    }

                    $tariff_times_form_fields[$name] = $form_field;
                    $this->tag->setDefault($name, $value);
                }
            }
        }

        list ($form_fields, $base_name) = $this->get_tariff_time_template();
        $tariff_times[] = $base_name;

        $this->tag->set_defaults($tariff_times_form_fields + $form_fields);

        $this->view->setVar('tariff_times', $tariff_times);
        $tariff_time_fields = array_keys($tariff_time_model->asArray());
        $tariff_time_fields[] = 'delete';
        $this->view->setVar('tariff_time_fields',  $tariff_time_fields);

        $template = $this->view->getVar('template');
        $template['java_scripts'][] = 'tariff_time.add.js';
        $this->view->setVar('template', $template);
        $this->view->setVar('days', $this->get_active_day_variants());
    }

    public function saveAction($id = 0) {
        if (!$this->request->isPost()) {
            $this->flash->error('Wrong request type');
            $this->dispatcher->forward([
                'action' => 'index'
            ]);
            return;
        }

        $model = $this->_model->findFirstById($id);
        if (!$model) {
            $this->flash->error('Wrong model id');
            $this->dispatcher->forward([
                'action' => 'index'
            ]);
            return;
        }
        $this->_saveFromRequest($model, [], FALSE);

        $id = $this->_last_saved_id;
        if (empty($id) || empty($this->request->getPost('tariff_time'))) {
            $this->dispatcher->forward([
                'action' => 'edit',
                'params' => [$id]
            ]);
            return;
        }

        $post_times = $this->request->getPost('tariff_time');
        foreach($post_times as $time) {
            $tariff_time = new TariffTime();
            if (isset($time['id'])) {
                $tariff_time = $tariff_time->findFirstById($time['id']);
                if (isset($time['delete'])) {
                    $tariff_time->delete();
                    continue;
                }
            }
            $time['tariff_id'] = $id;
            foreach (['active_time_from', 'active_time_till'] as $field) {
                $value = explode(':', $time[$field]);
                for ($i = 0; $i < 2; ++$i) {
                    if (isset($value[$i])) {
                        $value[$i] = intval($value[$i]);
                    } else {
                        $value[$i] = 0;
                    }
                }

                $time[$field] = $value[0] * 60 * 60 + $value[1] * 60;
            }
            if (!empty($tariff_time)) {
                $this->_saveFromRequest($tariff_time, $time, FALSE);
            }
        }

        $this->dispatcher->forward([
            'action' => 'edit',
            'params' => [$id]
        ]);
    }


}