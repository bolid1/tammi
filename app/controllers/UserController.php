<?php

use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;

class UserController extends ControllerModel {
	public function initialize() {
		$this->_model = new User();
		parent::initialize();

		unset($this->_table_fields['password']);
	}

	public function push_allAction() {

	}

	public function pushAction() {
		if (!$this->request->isPost()) {
			$this->flash->error('Wrong request method');

			return;
		}

		$type = $this->request->getPost('type');

		if (!in_array($type, ['prod', 'dev'])) {
			$this->flash->error('Wrong type');

			return;
		}

		$user_id = (int)$this->request->getPost('user_id');
		if ($type == 'dev' && $user_id <= 0) {
			$this->flash->error('Wrong user id');

			return;
		}

		$message = trim($this->request->getPost('message'));
		if (empty($message)) {
			$this->flash->error('Wrong message');

			return;
		}

		try {
			$push = new \Libs\Push\Apple($this->di->get('config'), $type);
		} catch (Exception $ex) {
			$this->flash->error('Runtime error: ' . $ex->getMessage());
			return;
		}

		if (empty($push)) {
			$this->flash->error('Server error');

			return;
		}

		$step = 100;

		if ($type === 'prod' && $user_id <= 0) {
			$parameters = [
				"os=:os:",
				'bind' => ['os' => User::OS_APPLE]
			];
		} else {
			$parameters = [
				"os = :os: AND id = :id:",
				'bind' => ['os' => User::OS_APPLE, 'id' => $user_id]
			];
		}

		$count = User::count($parameters);

		for ($counter = 0; $counter <= $count; $counter += $step) {
			$parameters['limit'] = ['number' => $step, 'offset' => $counter];
			$users = User::find($parameters);
			if (!empty($users)) {
				foreach ($users as $user) {
					/** @var User $user */
					if (empty($user->push_token) || !$push->send($user->push_token, $message)) {
						$this->flash->error('Send error');
					}
				}
			}
		}
	}
}
