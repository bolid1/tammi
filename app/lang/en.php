<?php
return [
    'id' => 'ID',
    'title' => 'Title',

    // Entities
    'city' => 'City',
    'company' => 'Company',
    'tariff' => 'Tariff',
    'user' => 'Devices',
    'users' => 'Users',

    // Actions
    'Index' => 'List',
    'Search' => 'Search',
    'New' => 'Create',
    'Edit' => 'Edit',
    'Delete' => 'Delete',

    // Tariffs
    'economy' => 'Economy',
    'business' => 'Business',
    'vip' => 'VIP',

    // Sms
    'sms-confirm-phone-number' => 'To complete registration in tammi input %code%',
];