<?php
return [
    'id' => 'ID',
    'title' => 'Название',

    // Entities
    'city' => 'Город',
    'company' => 'Компания',
    'tariff' => 'Тариф',
    'user' => 'Устройства',
    'users' => 'Пользователи',

    // Actions
    'Index' => 'Список',
    'Search' => 'Поиск',
    'New' => 'Создать',
    'Edit' => 'Редактировать',
    'Delete' => 'Удалить',

    // Tariffs
    'economy' => 'Эконом',
    'business' => 'Бизнес',
    'vip' => 'VIP',

    // Sms
    'sms-confirm-phone-number' => 'Код для завершения регистрации: %code%',
];