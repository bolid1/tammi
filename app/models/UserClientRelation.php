<?php

use Phalcon\Db\Column;

class UserClientRelation extends ModelBase {

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $user_id;

    /**
     *
     * @var integer
     */
    public $client_id;

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap() {
        return [
            'id' => 'id',
            'user_id' => 'user_id',
            'client_id' => 'client_id'
        ];
    }

    public function initialize() {
        $this->_fields = [
            'user_id' => [
                'type' => Column::TYPE_INTEGER,
            ],
            'client_id' => [
                'type' => Column::TYPE_INTEGER,
            ],
        ];

        $this->belongsTo('client_id', 'Client', 'id');
        $this->belongsTo('user_id', 'User', 'id');
    }

    public function beforeCreate() {
        if (empty($this->client_id)) {

        }
    }

    /**
     * @return Client
     */
    public function getClient() {
        var_dump($this->getRelated('Client'));
        die;
    }
}
