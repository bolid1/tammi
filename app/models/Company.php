<?php

class Company extends ModelBase {

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $title;

    /**
     *
     * @var string
     */
    public $description;

    /**
     *
     * @var string
     */
    public $site_url;

    /**
     *
     * @var string
     */
    public $logo_url;

    /**
     *
     * @var string
     */
    public $phone_call;

    /**
     *
     * @var string
     */
    public $phone_sms;

    /**
     *
     * @var string
     */
    public $app_prefix;

    /**
     *
     * @var integer
     */
    public $app_id;

    /**
     *
     * @var string
     */
    public $order_site_url;

    public function initialize() {
        parent::initialize();
        $this->hasMany("id", "Tariff", "company_id");
        $this->hasOne("id", "CompanyIntegration", "company_id");
    }

    public function asArray($columns = NULL, $constants_to_string = FALSE) {
        $result = parent::asArray($columns, $constants_to_string);
        if (empty($result['order_site_url'])) {
            $result['order_site_url'] = '';
        }

        return $result;
    }

    /**
     * @param array|null $parameters
     * @return bool|CompanyIntegration
     */
    public function getIntegration($parameters = NULL) {
        return $this->getRelated('CompanyIntegration', $parameters);
    }

}
