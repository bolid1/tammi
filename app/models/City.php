<?php

use \Phalcon\Mvc\Model\MetaDataInterface;

class City extends ModelBase {

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $title;


    public function initialize() {
        parent::initialize();
        $this->hasMany('id', 'Tariff', 'city_id');
    }
}
