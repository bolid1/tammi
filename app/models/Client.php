<?php

use Phalcon\Db\Column;

class Client extends ModelBase {

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $date_create;

    /**
     *
     * @var string
     */
    public $date_update;

    /**
     *
     * @var string
     */
    public $phone;

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap() {
        return [
            'id' => 'id',
            'date_create' => 'date_create',
            'date_update' => 'date_update',
            'phone' => 'phone'
        ];
    }

    public function beforeCreate() {
        //Установить дату создания
        $time_zone = new DateTimeZone('Europe/Moscow');
        $date_time = new DateTime('now', $time_zone);
        $this->date_create = $date_time->format('Y-m-d H:i:s');
    }

    public function initialize() {
        $this->_fields = [
            'date_create' => [
                'type' => Column::TYPE_DATETIME,
            ],
            'date_update' => [
                'type' => Column::TYPE_DATETIME,
            ],
        ];

        $this->hasOne('id', 'UserClientRelation', 'client_id');
    }

    public static function generateCode($user_id, $phone) {
        $hash_algorithms = [
            'md5', 'sha1', 'sha224',  'sha256',  'sha384',  'sha512'
        ];

        $key = $user_id . '-' . $phone;
        $code = '';
        foreach ($hash_algorithms as $hash_algorithm) {
            $code .= hash($hash_algorithm, $key);
        }

        $numbers = [];
        preg_match_all('(\d{1,3})', $code, $numbers);
        $numbers = array_map('intval', $numbers[0]);
        $code = array_sum($numbers);

        return $code;
    }
}
