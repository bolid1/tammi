<?php

class TariffTime extends ModelBase {

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $tariff_id;

    /**
     *
     * @var integer
     */
    public $active_day;

    /**
     *
     * @var integer
     */
    public $active_time_from;

    /**
     *
     * @var integer
     */
    public $active_time_till;


    public function initialize() {
        parent::initialize();
        $this->belongsTo("tariff_id", "Tariff", "id");

        $this->_fields_static_variants += [
            'active_day' => [
                1 => 'Monday',
                2 => 'Tuesday',
                3 => 'Wednesday',
                4 => 'Thursday',
                5 => 'Friday',
                6 => 'Saturday',
                7 => 'Sunday'
            ],
        ];
    }
}
