<?php
use Phalcon\Db\Column;

abstract class ModelBase extends \Phalcon\Mvc\Model {
    protected $_constants = [

    ];

    protected $_fields_static_variants = [

    ];

    protected $_fields = [

    ];

    protected $_linked = [

    ];

    public function initialize() {
    }

    public static function getList($parameters = NULL, $only_field = NULL) {
        $entities = self::find($parameters);
        /** @var Phalcon\Mvc\Model\Resultset\Simple $entities */
        $entities = $entities->toArray();

        $result = [];
        if (!is_null($only_field) && isset($entities[0]) && !isset($entities[0][$only_field])) {
            throw new InvalidArgumentException('Invalid only field value');
        }

        foreach ($entities as $entity) {
            $entity_id = $entity['id'];
            if (!is_null($only_field)) {
                if (empty($entity[$only_field])) {
                    $entity = NULL;
                } else {
                    $entity = $entity[$only_field];
                }
            }
            $result[$entity_id] = $entity;
        }

        return $result;
    }

    public function asArray($columns = NULL, $constants_to_string = FALSE) {
        $result = parent::toArray($columns);

        if ($constants_to_string) {
            foreach ($this->_constants as $column => $matches) {
                if (isset($result[$column])) {
                    if (isset($matches[$result[$column]])) {
                        $result[$column] = $matches[$result[$column]];
                    } else {
                        $result[$column] = 'undefined';
                    }
                }
            }
        }

        foreach ($this->_fields as $field_name => $field_option) {
            if (!isset($field_option['type'])) {
                continue;
            }

            switch ($field_option['type']) {
                case Column::TYPE_DECIMAL:
                    $result[$field_name] = empty($result[$field_name]) ? 0 : doubleval($result[$field_name]);
                    break;
                case Column::TYPE_INTEGER:
                    $result[$field_name] = empty($result[$field_name]) ? 0 : intval($result[$field_name]);
                    break;
                case Column::TYPE_BOOLEAN:
                    $result[$field_name] = empty($result[$field_name]) ? 0 : 1;
                    break;
            }
        }

        return $result;
    }

    /**
     * @param int $id
     * @return ModelBase
     */
    public function findFirstById($id) {
        return $this->findFirst(['id=:id:', 'bind' => ['id' => $id]]);
    }

    public function getId() {
        return empty($this->id) ? NULL : $this->id;
    }

    public function current($field_name){
        $result = $this->asArray(NULL, TRUE);

        $result = isset($result[$field_name]) ? $result[$field_name] : NULL;
        if (!is_null($result) && isset($this->_linked[$field_name])) {
            $result = $this->{$this->_linked[$field_name]};
            if (!empty($result->title)) {
                $result = $result->title;
            } else {
                $result = NULL;
            }
        }

        return $result;
    }

    public function getVariants($field_name) {
        $result = [];

        if (isset($this->_fields_static_variants[$field_name])) {
            $result = $this->_fields_static_variants[$field_name];
        }

        if (isset($this->_linked[$field_name])) {
            /** @var ModelBase $model */
            $model = new $this->_linked[$field_name]();
            $result = $model->find();
        }

        return $result;
    }
}