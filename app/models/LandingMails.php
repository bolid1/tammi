<?php

use Phalcon\Mvc\Model\Validator\Email as Email;

class LandingMails extends \Phalcon\Mvc\Model
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $email;

    /**
     *
     * @var integer
     */
    public $submitted;

    /**
     * Validations and business logic
     */
    public function validation()
    {

        $this->validate(
            new Email(
                array(
                    'field'    => 'email',
                    'required' => true,
                )
            )
        );
        if ($this->validationHasFailed() == true) {
            return false;
        }
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return array(
            'id' => 'id', 
            'email' => 'email', 
            'submitted' => 'submitted'
        );
    }

    public function send() {
        if (!empty($this->email)) {
            $submitted = mail('hello@tammiapp.ru', 'Новый клиент!', 'Email: ' . $this->email);
            $this->submitted = ($submitted ? 1 : 0);
            $this->save();
        }
    }
}
