<?php

use Phalcon\Db\Column;
use Phalcon\Mvc\Model\Relation;

class Tariff extends ModelBase {
    const TYPE_SUM = 1;
    const TYPE_MIN = 2;
    const TYPE_MAX = 3;

    const CLASS_ECONOMY = 1;
    const CLASS_BUSINESS = 2;
    const CLASS_VIP = 3;

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $company_id;

    /**
     *
     * @var integer
     */
    public $city_id;

    /**
     *
     * @var string
     */
    public $title;

    /**
     *
     * @var string
     */
    public $description;

    /**
     *
     * @var integer
     */
    public $type;

    /**
     *
     * @var integer
     */
    public $class;

    /**
     *
     * @var string
     */
    public $min_price;

    /**
     *
     * @var string
     */
    public $init_price;

    /**
     *
     * @var integer
     */
    public $init_time;

    /**
     *
     * @var integer
     */
    public $init_distance;

    /**
     *
     * @var integer
     */
    public $unit_time;

    /**
     *
     * @var string
     */
    public $price_time;

    /**
     *
     * @var integer
     */
    public $unit_distance;

    /**
     *
     * @var string
     */
    public $price_distance;

    public function initialize() {
        parent::initialize();
        $this->_fields_static_variants = $this->_constants = [
            'class' => [
                self::CLASS_ECONOMY => 'economy',
                self::CLASS_BUSINESS => 'business',
                self::CLASS_VIP => 'vip',
            ],
            'type' => [
                self::TYPE_SUM => 'sum',
                self::TYPE_MIN => 'min',
                self::TYPE_MAX => 'max',
            ]
        ];

        $this->_fields = [
            'init_price' => [
                'type' => Column::TYPE_DECIMAL,
            ],
            'min_price' => [
                'type' => Column::TYPE_DECIMAL,
            ],
            'init_distance' => [
                'type' => Column::TYPE_INTEGER,
            ],
            'init_time' => [
                'type' => Column::TYPE_INTEGER,
            ],
            'price_distance' => [
                'type' => Column::TYPE_DECIMAL,
            ],
            'unit_distance' => [
                'type' => Column::TYPE_INTEGER,
            ],
            'price_time' => [
                'type' => Column::TYPE_DECIMAL,
            ],
            'unit_time' => [
                'type' => Column::TYPE_INTEGER,
            ],
        ];

        $this->belongsTo('company_id', 'Company', 'id');
        $this->_linked['company_id'] = 'Company';
        $this->belongsTo('city_id', 'City', 'id');
        $this->_linked['city_id'] = 'City';
        $this->hasMany('id', 'TariffTime', 'tariff_id', [
            'foreignKey' => [
                'action' => Relation::ACTION_CASCADE,
            ]
        ]);

        $this->hasMany('id', 'Stats', 'max_tariff_id');
        $this->hasMany('id', 'Stats', 'min_tariff_id');
    }

    protected function calculate_price_unit($distance, $init_price, $init_distance, $price_distance, $unit_distance) {
        if ($unit_distance == 0) {
            return 0;
        }

        return $init_price + max($distance - $init_distance, 0) * $price_distance / $unit_distance;
    }

    public function calculate($distance, $duration) {
        $tariff = $this->asArray();
        $price_distance = $this->calculate_price_unit($distance, $tariff['init_price'], $tariff['init_distance'], $tariff['price_distance'], $tariff['unit_distance']);
        $price_time = $this->calculate_price_unit($duration, $tariff['init_price'], $tariff['init_time'], $tariff['price_time'], $tariff['unit_time']);

        switch (TRUE) {
            case $price_distance < 1:
                $price = $price_time;
                break;
            case $price_time < 1:
                $price = $price_distance;
                break;
            case $this->type == self::TYPE_MIN:
                $price = min($price_distance, $price_time);
                break;
            case $this->type == self::TYPE_SUM:
                // (- $this->init_price) - поправка на то, что init_price плюсуется в \Tariff::calculate_price_unit
                $price = $price_distance + $price_time - $this->init_price;
                break;
            case $this->type == self::TYPE_MAX:
                $price = max($price_distance, $price_time);
                break;
            default:
                $price = 0;
                break;
        }

        if (!empty($price)) {
            $price = round($price, 2);
        }

        $result = max($this->min_price, $price);

        return doubleval($result);
    }

    /**
     * @param array|null $parameters
     * @return \Phalcon\Mvc\Model\Resultset\Simple
     */
    public function getTimes($parameters = NULL) {
        return $this->getRelated('TariffTime', $parameters);
    }

    public function get_current_times() {
        $parameters = [
            "active_day=:active_day: AND active_time_from <= :seconds_from_day_start: AND active_time_till >= :seconds_from_day_start:",
            'bind' => [
                'active_day' => intval(date('N')),
                'seconds_from_day_start' => time() - strtotime('today'),
            ],
        ];

        return $this->getTimes($parameters);
    }

    /**
     * @param array|null $parameters
     * @return Company
     */
    public function getCompany($parameters = NULL) {
        return $this->getRelated('Company', $parameters);
    }

    /**
     * @param array|null $parameters
     * @return City
     */
    public function getCity($parameters = NULL) {
        return $this->getRelated('City', $parameters);
    }
}
