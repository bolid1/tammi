<?php

class Stats extends ModelBase {

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $date_create;

    /**
     *
     * @var integer
     */
    public $user_id;

    /**
     *
     * @var string
     */
    public $start_point;

    /**
     *
     * @var string
     */
    public $end_point;

    /**
     *
     * @var integer
     */
    public $distance;

    /**
     *
     * @var integer
     */
    public $time;

    /**
     *
     * @var integer
     */
    public $min_price;

    /**
     *
     * @var integer
     */
    public $min_tariff_id;

    /**
     *
     * @var integer
     */
    public $max_price;

    /**
     *
     * @var integer
     */
    public $max_tariff_id;

    public function initialize() {
        parent::initialize();
        $this->belongsTo('max_tariff_id', 'Tariff', 'id');
        $this->belongsTo('min_tariff_id', 'Tariff', 'id');
        $this->belongsTo('user_id', 'User', 'id');
    }

    public function beforeCreate() {
        //Установить дату создания
        $time_zone = new DateTimeZone('Europe/Moscow');
        $date_time = new DateTime('now', $time_zone);
        $this->date_create = $date_time->format('Y-m-d H:i:s');
    }
}
