<?php
use Phalcon\Db\Column;

class CompanyIntegration extends ModelBase {

    /**
     *
     * @var integer
     */
    public $company_id;

    /**
     *
     * @var integer
     */
    public $calculation;

    /**
     *
     * @var string
     */
    public $code;

    public function initialize() {
        parent::initialize();
        $this->_fields = [
            'company_id' => [
                'type' => Column::TYPE_INTEGER,
            ],
            'calculation' => [
                'type' => Column::TYPE_BOOLEAN,
            ],
        ];

        $this->hasOne("company_id", "Company", "id");
    }
}
