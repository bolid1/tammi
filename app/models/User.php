<?php

class User extends ModelBase {
    const OS_ANDROID = 1;
    const OS_APPLE = 2;

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var integer
     */
    public $os;

    /**
     *
     * @var string
     */
    public $push_token;

    public function initialize() {
        parent::initialize();
        $this->_constants = [
            'os' => [
                self::OS_ANDROID => 'android',
                self::OS_APPLE => 'apple',
            ],
        ];

        $this->hasMany('id', 'Stats', 'user_id');
        $this->hasMany('id', 'UserClientRelation', 'user_id');

    }

    public function get_os_list() {
        return $this->_constants['os'];
    }

    /**
     * @param array|null $parameters
     *
     * @return Client
     */
    public function getClient($parameters = NULL) {
        /** @var Phalcon\Mvc\Model\Resultset\Simple $client_relations */
        $client_relations = $this->getRelated('UserClientRelation', $parameters);

        if ($client_relations->count() > 0) {
            $client_relation = $client_relations->current();
        }

        if (empty($client_relation)) {
            $client_relation = new UserClientRelation();
            $client_relation->user_id = $this->id;
            $client_relation->save();
        }

        return $client_relation->getClient();
    }
}
