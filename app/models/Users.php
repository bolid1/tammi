<?php
use Phalcon\Mvc\Model\Validator\Email as Email;

class Users extends ModelBase
{

    /**
     *
     * @var integer
     */
    public $id;

    /**
     *
     * @var string
     */
    public $email;

    /**
     *
     * @var string
     */
    public $password;

    /**
     *
     * @var integer
     */
    public $active;

    /**
     *
     * @var string
     */
    public $role;

    public function validation() {
        parent::initialize();

        $this->validate(
            new Email(
                [
                    'field' => 'email',
                    'required' => TRUE,
                ]
            )
        );
        if ($this->validationHasFailed() == TRUE) {
            return FALSE;
        }

        return TRUE;
    }
}
