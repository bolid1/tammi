<?php 

use Phalcon\Db\Column;
use Phalcon\Db\Index;
use Phalcon\Db\Reference;
use /** @noinspection PhpUndefinedClassInspection */
    Phalcon\Mvc\Model\Migration;

/** @noinspection PhpUndefinedClassInspection */
class UserMigration_103 extends Migration
{

    public function up()
    {
        /** @noinspection PhpUndefinedMethodInspection */
        $this->morphTable(
            'user',
            array(
            'columns' => array(
                new Column(
                    'id',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'unsigned' => true,
                        'notNull' => true,
                        'autoIncrement' => true,
                        'size' => 10,
                        'first' => true
                    )
                ),
                new Column(
                    'os',
                    array(
                        'type' => Column::TYPE_INTEGER,
                        'unsigned' => true,
                        'size' => 2,
                        'after' => 'id'
                    )
                ),
                new Column(
                    'push_token',
                    array(
                        'type' => Column::TYPE_VARCHAR,
                        'size' => 120,
                        'after' => 'os'
                    )
                )
            ),
            'indexes' => array(
                new Index('PRIMARY', array('id'))
            ),
            'options' => array(
                'TABLE_TYPE' => 'BASE TABLE',
                'AUTO_INCREMENT' => '1945',
                'ENGINE' => 'InnoDB',
                'TABLE_COLLATION' => 'utf8_unicode_ci'
            )
        )
        );
    }
}
