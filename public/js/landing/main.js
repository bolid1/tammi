jQuery(document).ready(function ($) {

    'use strict';


    //SMOOTH SCROLL
    smoothScroll.init({
        speed: 500, // How fast to complete the scroll in milliseconds
        easing: 'easeInOutCubic', // Easing pattern to use
        updateURL: false, // Boolean. Whether or not to update the URL with the anchor hash on scroll
        callbackBefore: function (toggle, anchor) {
        }, // Function to run before scrolling
        callbackAfter: function (toggle, anchor) {
        } // Function to run after scrolling
    });


    //ONSCROLL ANIMATIONS
    var wow = new WOW(
        {
            boxClass: 'wow',      // animated element css class (default is wow)
            animateClass: 'animated', // animation css class (default is animated)
            offset: 0,          // distance to the element when triggering the animation (default is 0)
            mobile: false,       // trigger animations on mobile devices (default is true)
            live: true        // act on asynchronously loaded content (default is true)
        }
    );
    wow.init();


    // Scroll to subscribe module
    $(".lj-product-button-right").on('click', function (e) {
        e.preventDefault();
        $('html,body').animate({
                scrollTop: $(".join-now").offset().top
            },
            1250);
        setTimeout(function () {
            $('input#join-now-name').focus();
        }, 1250);
    });

    //SCROLL EFFECTS
    var controller;
    {
        // init controller
        var controller = new ScrollMagic.Controller();


        // DEVICE 1
        var scene = new ScrollMagic.Scene({
            triggerElement: "#trigger1",
            duration: 650,
            offset: -140,
            triggerHook: "onLeave"
        })
            .setPin("#pin1")
            .addTo(controller);

        // detect if mobile browser. regex -> http://detectmobilebrowsers.com
        var isMobile = (function (a) {
            return /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))
        })(navigator.userAgent || navigator.vendor || window.opera);

        // we'd only like to use iScroll for mobile...
        if (isMobile) {
            // configure iScroll
            var myScroll = new IScroll('#pin1',
                {
                    // don't scroll horizontal
                    scrollX: false,
                    // but do scroll vertical
                    scrollY: true,
                    // show scrollbars
                    scrollbars: true,
                    // deactivating -webkit-transform because pin wouldn't work because of a webkit bug: https://code.google.com/p/chromium/issues/detail?id=20574
                    // if you dont use pinning, keep "useTransform" set to true, as it is far better in terms of performance.
                    useTransform: false,
                    // deativate css-transition to force requestAnimationFrame (implicit with probeType 3)
                    useTransition: false,
                    // set to highest probing level to get scroll events even during momentum and bounce
                    // requires inclusion of iscroll-probe.js
                    probeType: 3,
                    // pass through clicks inside scroll container
                    click: true
                }
            );

            // overwrite scroll position calculation to use child's offset instead of container's scrollTop();
            controller.scrollPos(function () {
                return -myScroll.y;
            });

            // thanks to iScroll 5 we now have a real onScroll event (with some performance drawbacks)
            myScroll.on("scroll", function () {
                controller.update();
            });

            // add indicators to scrollcontent so they will be moved with it.
            scene.addIndicators({parent: ".scrollContent"});
        } else {

        }

        // DEVICE 2
        var scene = new ScrollMagic.Scene({
            triggerElement: "#trigger2",
            duration: 250,
            offset: -140,
            triggerHook: "onLeave"
        })
            .setPin("#pin2")
            .addTo(controller);

        // detect if mobile browser. regex -> http://detectmobilebrowsers.com
        var isMobile = (function (a) {
            return /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))
        })(navigator.userAgent || navigator.vendor || window.opera);

        // we'd only like to use iScroll for mobile...
        if (isMobile) {
            // configure iScroll
            var myScroll = new IScroll('#pin2',
                {
                    // don't scroll horizontal
                    scrollX: false,
                    // but do scroll vertical
                    scrollY: true,
                    // show scrollbars
                    scrollbars: true,
                    // deactivating -webkit-transform because pin wouldn't work because of a webkit bug: https://code.google.com/p/chromium/issues/detail?id=20574
                    // if you dont use pinning, keep "useTransform" set to true, as it is far better in terms of performance.
                    useTransform: false,
                    // deativate css-transition to force requestAnimationFrame (implicit with probeType 3)
                    useTransition: false,
                    // set to highest probing level to get scroll events even during momentum and bounce
                    // requires inclusion of iscroll-probe.js
                    probeType: 3,
                    // pass through clicks inside scroll container
                    click: true
                }
            );

            // overwrite scroll position calculation to use child's offset instead of container's scrollTop();
            controller.scrollPos(function () {
                return -myScroll.y;
            });

            // thanks to iScroll 5 we now have a real onScroll event (with some performance drawbacks)
            myScroll.on("scroll", function () {
                controller.update();
            });

            // add indicators to scrollcontent so they will be moved with it.
            scene.addIndicators({parent: ".scrollContent"});
        } else {

        }


        //scene.addIndicators();
    }
    ;


    //OWLCAROUSEL TESTIMONIAL CAROUSEL
    var owl = $("#testimonial-slider");

    owl.owlCarousel({
        itemsCustom: [
            [0, 1],
            [450, 1],
            [600, 1],
            [700, 2],
            [1000, 3],
            [1200, 3],
            [1600, 3]
        ],
        navigation: false
    });


    //CHART POLAR AREA
    var polarData = [
        {
            value: 300,
            color: "#F7464A",
            highlight: "#FF5A5E",
            label: "Red"
        },
        {
            value: 50,
            color: "#46BFBD",
            highlight: "#5AD3D1",
            label: "Green"
        },
        {
            value: 100,
            color: "#FDB45C",
            highlight: "#FFC870",
            label: "Yellow"
        },
        {
            value: 40,
            color: "#949FB1",
            highlight: "#A8B3C5",
            label: "Grey"
        },
        {
            value: 120,
            color: "#4D5360",
            highlight: "#616774",
            label: "Dark Grey"
        }

    ];


    //CHART DOUGHNUT
    var doughnutData = [
        {
            value: 300,
            color: "#FF2A68",
            highlight: "#FF2A68",
            label: "Maximus"
        },
        {
            value: 50,
            color: "#C644FC",
            highlight: "#C644FC",
            label: "Faucibus"
        },
        {
            value: 100,
            color: "#5AC8FB",
            highlight: "#5AC8FB",
            label: "Efficitur"
        },
        {
            value: 40,
            color: "#949FB1",
            highlight: "#949FB1",
            label: "Accumsan"
        },
        {
            value: 100,
            color: "#4D5360",
            highlight: "#4D5360",
            label: "Congue"
        }

    ];

    window.onload = function () {
        var element, ctx;

        element = document.getElementById("doughnut");
        if (element != null) {
            ctx = element.getContext("2d");
            window.myDoughnut = new Chart(ctx).Doughnut(doughnutData, {
                responsive: true
            });
        }

        element = document.getElementById("polar-area");
        if (element != null) {
            ctx = element.getContext("2d");
            window.myPolarArea = new Chart(ctx).PolarArea(polarData, {
                responsive: true
            });
        }
    };


    //FIX HOVER EFFECT ON IOS DEVICES
    document.addEventListener("touchstart", function () {
    }, true);


});


$(window).load(function () {

    //PRELOADER
    $('#preload').delay(350).fadeOut('slow'); // will fade out the white DIV that covers the website.

});


/* NEWSLETTER FORM FUNCTION */
var newsletter_send = function () {

    'use strict';

    var $email_container = $("#newsletter_email"),
        email = $email_container.val();

    if (email == "") {
        alert("Вы не указали email!");
        $email_container.focus();
    } else {
        $.post($email_container.data('send_to'), {email: email}, function (data) {
            if (data.status === "success") {
                alert("Спасибо! Мы вам скоро напишем.");
                setTimeout(function () {
                    $email_container.val("");
                }, 3000);
            } else if (data.status === "invalid") {
                alert("Ошибка. Неверный e-mail.");
                $email_container.focus();
            } else if (data.status === "exist") {
                alert("Ошибка. Вы уже оставляли свой адрес.");
                $email_container.focus();
            } else {
                alert("Ошибка. Что-то пошло не так :(");
                $email_container.focus();
            }
        });
    }

};
	

