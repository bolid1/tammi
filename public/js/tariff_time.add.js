(function () {
    var Controller;

    Controller = function () {
        var _this = this;
        _this.iterator = 1;

        this.appendTimes = function () {
            $('.js-day').each(function (key, checkbox) {
                if (checkbox.checked) {
                    _this.appendTime(checkbox.value);
                }
            });
        };

        this.appendTime = function (day) {
            var $base_new_element = _this.getBaseElement(),
                template = $("<div></div>").append($base_new_element.clone()).html(),
                search = 'tariff_time[new]',
                replace = 'tariff_time[new_' + _this.iterator + ']',
                $template, $active_day_select, $active_day;

            while(template.indexOf(search) >= 0) {
                template = template.replace(search, replace);
            }

            $template = $(template);
            $template.removeClass('js-tariff_time_new');

            $base_new_element.find('input').each(function (key, input) {
                var $input = $(input),
                    name = $input.attr('name').replace(search, replace);

                $template.find('input[name="' + name + '"]').val($input.val())
            });

            $active_day_select = $template.find("select[name='" + replace + "[active_day]']");
            if ($active_day_select.val()) {
                $active_day_select.find(':selected').removeAttr('selected');
            }
            $active_day = $active_day_select.find("[value='" + day + "']");
            $active_day.attr('selected', 'selected');
            $active_day.change();

            $base_new_element.before($template);

            _this.iterator++;
        };

        this.deleteTime = function (e) {
            var $btn = $(e.target),
                $wrapper = $btn.closest('.js-tariff_time:not(.js-tariff_time_new)');

            $wrapper.remove();

            e.preventDefault();
        };

        this.getBaseElement = function () {
            return $('.js-tariff_time_new');
        };

        this.destroy = function (){
            var need_confirm = false;
            $('.js-delete').each(function (key, checkbox) {
                need_confirm = need_confirm || checkbox.checked;
            });

            if (need_confirm && !confirm('Выделены объекты для удаления, продолжить?')) {
                return false;
            }
            _this.getBaseElement().remove();

            $(document)
                .off('click', '#add_time_btn')
                .off('submit', '#tariff_form');
        };


        _this.getBaseElement().css('background-color', 'cadetblue');

        $(document)
            .on('click', '#add_time_btn', this.appendTimes)
            .on('submit', '#tariff_form', _this.destroy)
            .on('click', '.js-delete-new', _this.deleteTime);
    };

    Controller();
})();
